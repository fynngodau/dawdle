– Add choice results
– Add overview of supported modules
– Handle unavailable API calls (for old Moodle versions)
– Make text selectable and other small UI changes
– Login improvements, new workarounds and documentation
– Improved network change detection
– More specific network error messages
