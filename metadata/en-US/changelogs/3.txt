– Fix crash on Android 5 upon login
– Fix instance detection issue while logging in
– Fix web browser login issue
– Add some further translations
