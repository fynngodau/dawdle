Just in time for the winter term 2022/2023, new features are coming to the dawdle app.

– Course search and enrollment
– First steps toward tablet support
– Improved backwards navigation behavior
– Show large headers in course overview
– Add course information view
– Many bug fixes
– Feature preview: show grade on assignment
