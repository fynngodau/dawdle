Material in these screenshots is licensed under permissive CC licenses.
From https://school.moodledemo.net.

* Class and Conflict in World Cinema: CC BY-SA by Mary Cooch and Chris Cooch
* "English: The Lake Poets": CC BY-SA by Mary Cooch, and including public domain content
* "History: Russia in Revolution": CC BY-SA by Estelle Cooch
* Mystère à Hyères: CC BY-SA by Estelle Cooch
* Psychology in Cinema: CC BY-SA by Chris Cooch

Some material was adapted for better screenshot fit. The screenshots themselves are licensed CC BY-SA 4.0 International by Fynn Godau.
