Fynn Godau
Stößelstraße 6
97422 Schweinfurt
Deutschland

fynngodau@mailbox.org
+49 9721 730335

Umsatzsteuer-Identifikationsnummer (VAT identification number): DE347187327

Plattform der EU zur außergerichtlichen Streitbeilegung
(EU platform for Online Dispute Resolution):
https://ec.europa.eu/consumers/odr/