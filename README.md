# dawdle


This Moodle client is a fork of the Android version of the CMS BPHC app.
These are its most prominent features:

* Custom instance by URL
* Basic auth and website auth
* View enrolled courses
* Offline cache
* Calendar link generation
* Dark theme
* In-app user help menu

The following Moodle modules can be viewed; unsupported modules open in the browser.

* URL
* Page (including image rendering)
* File
* Folder
* Forum (threads and discussions, but not posting)
* Choice (viewing options, submitting, undoing, and viewing results)
* Zoom (opening conference links in external application)

All modules (except for Zoom) store their data for offline usage.

When opening Moodle links externally, magic login is performed to log in on the browser.

More modules will be added in the future.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/godau.fynn.moodledirect/)

## License

This project, as a whole, is licensed under the terms of the **GNU GPL v3 or later**. However, to allow the maintainers
of the upstream project to benefit from my work, I, Fynn Godau, the copyright owner of most exclusively
GPL-licensed content that this project contains, grant the following **exceptions** per section 7 of the GNU GPL:

* The work covered by this license may be added to the *main* CMS-Android project,
  in a way that integrates into its user experience, under the terms of the MIT license.
* The work covered by this license may be used as a basis for future versions of the *main* CMS-Android project
  under the terms of the MIT license.
* The *main* CMS-Android project refers to the repository located at https://github.com/crux-bphc/CMS-Android/.
* By anyone who intends to contribute to the *main* CMS-Android project, the work covered by
  this license may be modified and adapted for the purpose of achieving the above two goals. This includes,
  among others, copying material into a fork of the aforementioned project, working on this copy and opening a pull request.
* It follows that once any source code is copied into the *main* CMS-Android project, that source code, in the potentially
  adapted form in which it is added to that project, shall be governed by the terms of the MIT license. However, other portions of the dawdle
  project are not affected and continue to be made available under this license.

**Many thanks to the authors of the CMS-Android project for their free software work that this project is based on.**

## Contributing

**Code contributions.** As a good-spirited open source project, dawdle of course welcomes code
contributions of all kinds. Pick up some issues and open some Pull Requests!

Your code needs to be licensed under either the GNU GPL v3+ with the exception denoted above
or under a more permissive license.


**Translations.** The project welcomes translations through a [translation platform](https://translate.codeberg.org/projects/dawdle/). Note that like when commiting with git, your
email address will be included in the commit history. Each contributor gains a spot in the About menu.

**Donations.** You may [donate](DONATING.md) to the project.

**Tell a friend.** dawdle is only available on F-Droid. For this reason, it would help a lot if
you told your friends about the app, and taught them how they can install it if necessary.

**Discuss issues.** What features would be useful to you? What bugs have you encountered? Your
thoughts about the app are valuable! Please share them in the [issue tracker](https://codeberg.org/fynngodau/dawdle/issues).
You can contribute by submitting an issue, or by picking an existing one from the list and sending a PR. Ensure that the branch underlying the PR is up-to-date with latest changes. Contributors must ensure they add any significant changes to the [changelog](CHANGELOG.md) in the same PR itself.

**Be a reference.** For increased exposure and official recognition, dawdle hopes to become a
Moodle Certified Integration and needs client references. If your company or university is using the application
as an institution, please approach fynngodau@mailbox.org and provide contact details.
