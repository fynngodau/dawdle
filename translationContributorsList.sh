#!/usr/bin/bash

for dir in app/src/main/res/values-*
do
	echo $(echo $dir | sed "s/.*-//"):
	{
		git log --format="	%an" --follow $dir/strings.xml
	} | sort | uniq | sed "/Weblate\|Fynn Godau/d"
done
