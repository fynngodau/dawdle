package godau.fynn.moodledirect.activity.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;

public class DebugFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_debug, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button exportUrl = view.findViewById(R.id.export_url);
        TextView text = view.findViewById(R.id.text);

        exportUrl.setOnClickListener(v -> {
            ExceptionHandler.tryAndThenThread(
                    () -> {
                        StringBuilder result = new StringBuilder();
                        MoodleDatabase.Dispatch moodle = MyApplication.moodle().forceOffline();
                        for (Course course : moodle.getCore().getCourses()) {
                            result.append("\n=== ").append(course.name).append(" ===").append("\n\n");
                            try {
                                for (Module module : moodle.getCourseContent().getCourseModules(course.id)) {
                                    result.append("* ").append(module.getName());
                                    if (module.getModuleType() == ResourceType.URL
                                            && module.getFileList().size() > 0
                                            && !module.getFileList().get(0).url.isEmpty()) {
                                        result.append(": ").append(module.getFileList().get(0).url);
                                    } else if (module.url != null) {
                                        result.append(": ").append(module.url);
                                    }
                                    result.append("\n");
                                }
                            } catch (OfflineException e) {
                                // noop
                            }
                        }
                        return result.toString();
                    }, result -> {
                        Log.d("Debug export", result);
                        text.setText(result);
                        text.setVisibility(View.VISIBLE);
                    },
                    requireContext()
            );
        });
    }
}
