package godau.fynn.moodledirect.util.spanner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.text.style.URLSpan;
import android.util.Log;
import com.sysdata.htmlspanner.SpanStack;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.util.Constants;
import godau.fynn.moodledirect.util.Util;
import org.htmlcleaner.TagNode;

public class VideoHandler extends com.sysdata.htmlspanner.handlers.LinkHandler {

    private final Context context;

    public VideoHandler(Context context) {
        this.context = context;
    }

    @Override
    public void handleTagNode(TagNode node, SpannableStringBuilder builder, int start, int end, SpanStack spanStack) {

        String title = null;

        if (node.hasAttribute("title")) {
            title = node.getAttributeByName("title");
            if (title.isEmpty()) title = null;
        }

        TagNode sourceNode = node.findElementByName("source", false);
        String src;
        if (sourceNode != null) {
            src = sourceNode.getAttributeByName("src");
        } else {
            // A video without a source?
            Log.w(VideoHandler.class.getSimpleName(), "Encountered video without source tag! Looking for fallback link.");

            TagNode linkNode = node.findElementByName("a", true);

            if (linkNode != null) {
                src = linkNode.getAttributeByName("href");
            } else {
                // A video with neither source nor a link content??
                Log.w(VideoHandler.class.getSimpleName(), "No fallback link found. Looking at content text.");

                String text = node.getText().toString();
                if (text.startsWith("http://") || text.startsWith("https://")) src = text;
                else {
                    Log.e(VideoHandler.class.getSimpleName(), "Video's body is not a source link. Giving up.");
                    builder.append(node.getText());
                    return;
                }
            }
        }

        if (title == null) title = src;

        // Play icon
        builder.append("\uFFFC ")
                .append(title);
        ImageSpan imageSpan = new ImageSpan(loadDrawable());
        spanStack.pushSpan(imageSpan, start, start + 1);

        // Fill absolute paths directly; relative paths are handled in ImageLoaderTextView
        if (src.startsWith("http://") || src.startsWith("https://")) {

            // Media link
            if (src.startsWith(Constants.API_URL + "webservice/pluginfile.php") || src.startsWith(Constants.API_URL + "pluginfile.php")) {
                String privateAccessKey = new PreferenceHelper(context).getUserAccount().getFilePrivateAccessKey();
                src = src.replace("/webservice/pluginfile.php/", "/pluginfile.php/")
                        .replace("/pluginfile.php/",
                                "/tokenpluginfile.php/" +
                                        privateAccessKey + "/"
                        );
            }

            spanStack.pushSpan(new URLSpan(src), start, start + 1);
            spanStack.pushSpan(new URLSpan(src), start + 2, builder.length());
        } else {
            // Push video span to be replaced later
            spanStack.pushSpan(new VideoSpan(src), start, start + 1);
            // Do not linkify space for aesthetics
            spanStack.pushSpan(new VideoSpan(src), start + 2, builder.length());
        }
    }

    @Override
    public boolean rendersContent() {
        return true;
    }

    public Drawable loadDrawable() {
        Drawable drawable = context.getDrawable(R.drawable.ic_play);
        int size = Util.spToPx(16, context);
        drawable.setBounds(0, 0, size, size);
        return drawable;
    }
}
