package godau.fynn.moodledirect.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import androidx.annotation.UiThread;
import androidx.annotation.WorkerThread;
import androidx.appcompat.app.AlertDialog;
import androidx.documentfile.provider.DocumentFile;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.module.FileManager;
import godau.fynn.moodledirect.module.files.HasFiles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Convenience method for access to {@link FileManager}
 */
public class FileManagerWrapper {

    public static final int REQUEST_OPEN_DIRECTORY = 346; //DIR
    private final FileManager fileManager;
    private final Context context;

    /**
     * Track which files are currently being downloaded
     */
    private static final Map<File, Thread> fileThreadMap = new HashMap<>();

    public FileManagerWrapper(FileManager fileManager, Context context) {
        this.fileManager = fileManager;
        this.context = context;
    }

    public static Intent requestPermissionIntent() {
        return new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
    }

    public static void onRequestPermissionResult(Intent data, Context context) {
        if (data != null) {
            Uri uri = data.getData();

            final int takeFlags = data.getFlags()
                    & (Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            // Accept offer to persist this permission (noinspection WrongConstant)
            context.getContentResolver().takePersistableUriPermission(uri, takeFlags);

            new PreferenceHelper(context).setDownloadPath(uri.toString());
        }
    }

    @WorkerThread
    public Uri getLocation(File file) {
        return fileManager.getLocation(file);
    }

    @WorkerThread
    public void addDownloadStatus(List<? extends HasFiles> listHasFiles) {
        if (listHasFiles == null) return;
        List<File> files = listHasFiles.stream()
                        .filter(hasFiles -> {
                            if (hasFiles instanceof Module) return ((Module) hasFiles).isDownloadable();
                            else return true;
                        })
                        .flatMap(hasFiles -> {
                            if (hasFiles.getFileList() != null) return hasFiles.getFileList().stream();
                            else return Stream.empty();
                        })
                        .collect(Collectors.toList());

        // Avoid too big queries to addDownloadStatus
        int i = 0, last = 0;
        do {
            i = Math.min(i + 30, files.size());

            fileManager.addDownloadStatus(files.subList(last, i));

            last = i;
        } while (i < files.size());


        for (File file : files) {
            if (fileThreadMap.containsKey(file)) {
                file.downloadStatus = FileManager.DownloadStatus.DOWNLOADING;
            }
        }
    }

    /**
     * Creates intent to open the provided file.
     *
     * @param file         File to be opened
     * @param doesNotExist To be run if desired file does not exist
     * @param context      Context from which to spawn AlertDialog
     */
    @UiThread
    public void openFile(File file, Runnable doesNotExist, Context context) {
        ExceptionHandler.tryAndThenThread(
                () -> fileManager.getLocation(file),
                location -> {
                    if (location == null) {
                        doesNotExist.run();
                    } else {

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(location, file.mimetype);
                        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(Intent.EXTRA_TITLE, file.filename);
                        try {
                            if (intent.resolveActivity(context.getPackageManager()) == null) throw new ActivityNotFoundException();
                            context.startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            new AlertDialog.Builder(context)
                                    .setTitle(R.string.error_activity_not_found_title)
                                    .setMessage(context.getString(R.string.error_activity_not_found_message, file.mimetype))
                                    .show();
                        }
                    }
                },
                exception -> {},
                context
        );
    }

    @UiThread
    public boolean startDownload(File file, String courseShortName, Context context, Consumer<Uri> then) {
        return startDownload(file, courseShortName, null, context, then);
    }

    @UiThread
    public boolean startDownload(File file, String courseShortName, String moduleName, Context context, Consumer<Uri> then) {
        Log.d(FileManagerWrapper.class.getSimpleName(), "Attempting to start download for: " + file.url +
                " (" + file.filename + ") ");

        // Download path set?
        if (fileManager.getDownloadUri() == null) {

            new AlertDialog.Builder(context)
                    .setTitle(R.string.download_path_dialog_title)
                    .setMessage(R.string.download_path_dialog_message)
                    .setPositiveButton(R.string._continue, (dialog, which) ->
                                    ((Activity) context)
                                            .startActivityForResult(requestPermissionIntent(), REQUEST_OPEN_DIRECTORY)
                    )
                    .setNegativeButton(R.string._cancel, null)
                    .show();
            Log.d(FileManagerWrapper.class.getSimpleName(), "No storage location configured, can't download.");
            return false;
        }

        // Download path available?
        DocumentFile downloadDirectory = DocumentFile.fromTreeUri(context, fileManager.getDownloadUri());
        if (
                downloadDirectory == null || !downloadDirectory.exists()
        ) {
            new AlertDialog.Builder(context)
                    .setTitle(R.string.download_path_unavailable_dialog_title)
                    .setMessage(R.string.download_path_unavailable_dialog_message)
                    .setPositiveButton(R.string.download_path_unavailable_dialog_change, (dialog, which) ->
                            ((Activity) context)
                                    .startActivityForResult(requestPermissionIntent(), REQUEST_OPEN_DIRECTORY)
                    )
                    .setNeutralButton(R.string.download_path_unavailable_dialog_info, (dialog, which) ->
                            new AlertDialog.Builder(context).setMessage(
                                    context.getString(
                                            R.string.download_path_unavailable_info_dialog_message,
                                            fileManager.getDownloadUri().toString()
                                    )
                            ).show()
                    )
                    .setNegativeButton(R.string._cancel, null)
                    .show();
            Log.d(FileManagerWrapper.class.getSimpleName(), "Download path is no longer available, can't download.");
            return false;
        }

        file.downloadStatus = FileManager.DownloadStatus.DOWNLOADING;

        Thread thread = ExceptionHandler.tryAndThenThread(
                () -> fileManager.download(file, courseShortName, moduleName),
                fileUri -> {
                    file.downloadStatus = FileManager.DownloadStatus.DOWNLOADED;
                    fileThreadMap.remove(file);
                    then.accept(fileUri);
                    Log.d(FileManagerWrapper.class.getSimpleName(), "Download of " + file.url + " successful.");
                },
                otherwise -> {
                    file.downloadStatus = FileManager.DownloadStatus.FAILED;
                    fileThreadMap.remove(file);
                    then.accept(null);
                    Log.d(FileManagerWrapper.class.getSimpleName(), "Download of " + file.url + " failed.");
                },
                context
        );

        file.downloadStatus = FileManager.DownloadStatus.DOWNLOADING;
        fileThreadMap.put(file, thread);

        return true;
    }

}
