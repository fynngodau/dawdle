package godau.fynn.moodledirect.util;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;

import androidx.annotation.AttrRes;
import androidx.annotation.ColorInt;
import androidx.annotation.IntRange;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.util.spanner.HtmlSpanner;
import godau.fynn.moodledirect.view.ExpandableTextDisplay;
import godau.fynn.moodledirect.view.ImageLoaderTextView;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class TextUtil {

    private static final TypedValue selectableItemBackground = new TypedValue();

    public static SpannableStringBuilder trimEndingNewlines(Spanned spanned) {
        int newEnd = spanned.length();
        for (int i = spanned.length() - 1; i >= 0; i--) {
            char c = spanned.charAt(i);
            if (c == '\u0000')
                continue;
            else if (c == '\n') {
                newEnd = i;
            } else {
                return new SpannableStringBuilder(spanned, 0, newEnd);
            }
        }
        return new SpannableStringBuilder(spanned);
    }

    public static boolean hasContent(CharSequence charSequence) {
        for (int i = 0; i < charSequence.length(); i++) {
            char c = charSequence.charAt(i);
            if (
                    c != '\u0000' && c != ' ' && c != '\n' && c != ' '
            ) return true;
        }
        return false;
    }

    /**
     * @param targetLength Roughly the intended length of the result. Must be greater than 70.
     *                     Will be exceeded by +30 at most.
     */
    public static CharSequence crop(CharSequence text, @IntRange(from = 71) int targetLength) {

        if (text.length() <= targetLength + 31) {
            return text;
        }

        // Seek full stop within [number - 70; number + 30]
        for (int i = targetLength + 30; i > targetLength - 70; i--) {
            if (text.charAt(i) == '.') {
                return TextUtils.concat(text.subSequence(0, i + 1), " …");
            }
        }

        // Seek ':' and ';' within [number - 70; number + 30]
        for (int i = targetLength + 30; i > targetLength - 70; i--) {
            if (text.charAt(i) == ':' || text.charAt(i) == ';') {
                return TextUtils.concat(text.subSequence(0, i + 1), " …");
            }
        }

        // Seek space within [number - 30; number]
        for (int i = targetLength; i > targetLength - 30; i--) {
            if (text.charAt(i) == ' ' || text.charAt(i) == ' ') {
                return TextUtils.concat(text.subSequence(0, i), " …");
            }
        }

        // Break at number
        return TextUtils.concat(text.subSequence(0, targetLength), " …");
    }

    public static ForegroundColorSpan createColorAttributeSpan(Context context, @AttrRes int attr) {
        // Get attr for text color
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[] { attr };
        TypedArray array = context.obtainStyledAttributes(typedValue.data, textSizeAttr);
        @ColorInt int textColor = array.getColor(0, -1);
        array.recycle();

        return new ForegroundColorSpan(textColor);
    }

    /**
     * Sets up the provided TextView, which is a (not necessarily direct) descendant of the {@code recyclerView},
     * to be expandable. Smooth animation included – thanks to Bubu for rubberducking.
     */
    public static void setTextExpandable(ImageLoaderTextView textView, ExpandableTextDisplay display, int position,
                                         RecyclerView recyclerView, Context context) {
        if (!display.isExpanded && display.text.length() > 280) {

            textView.setText(new SpannableStringBuilder(crop(display.text, 250)),
                    display.files, MyApplication.moodle().getDispatch().getCommonAsset(),
                    context.getResources().getDisplayMetrics());

            textView.getContext().getTheme()
                    .resolveAttribute(android.R.attr.selectableItemBackground, selectableItemBackground, true);
            textView.setBackgroundResource(selectableItemBackground.resourceId);

            textView.setTextIsSelectable(false);


            textView.setOnClickListener(v -> {
                textView.setText(display.text, display.files,
                        MyApplication.moodle().getDispatch().getCommonAsset(),
                        context.getResources().getDisplayMetrics()
                );
                recyclerView.getAdapter().notifyItemChanged(position);
                display.isExpanded = true;
                textView.setOnClickListener(null);
                textView.setClickable(false);
                textView.setMovementMethod(LinkMovementMethod.getInstance());
                textView.setTextIsSelectable(true);

                textView.setBackground(null);
            });

        } else {
            textView.setText(new SpannableStringBuilder(display.text),
                    display.files, MyApplication.moodle().getDispatch().getCommonAsset(),
                    context.getResources().getDisplayMetrics()
            );
            textView.setOnClickListener(null);
            textView.setClickable(false);
            textView.setMovementMethod(LinkMovementMethod.getInstance());
            textView.setBackground(null);
            textView.setTextIsSelectable(true);
        }
    }

    @Deprecated
    public static SpannableStringBuilder fromHtml(String s) {
        return trimEndingNewlines(
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                        Html.fromHtml(s, Html.FROM_HTML_OPTION_USE_CSS_COLORS)
                        : Html.fromHtml(s)
        );
    }

    /**
     * @param context used to load better placeholder images and to process screen width
     */
    public static SpannableStringBuilder fromHtml(String s, Context context) {
        return trimEndingNewlines(
            HtmlSpanner.get(
                    context,
                    context.getResources().getDisplayMetrics().widthPixels
            ).fromHtml(s)
        );
    }


    /**
     * @param context used to load better placeholder images and to process screen width
     */
    public static SpannableStringBuilder fromHtml(String s, Context context, int width) {
        return trimEndingNewlines(
                HtmlSpanner.get(
                        context, width
                ).fromHtml(s)
        );
    }

    public static String combine(String delimiter, String... items) {
        return Arrays.stream(items)
                .filter(Objects::nonNull)
                .filter(s -> !s.isEmpty())
                .collect(Collectors.joining(delimiter));
    }
}
