package godau.fynn.moodledirect.util;

/**
 * Constants and quasi-constants that do not usually change while the app is running.
 *
 * @author Created by harsu on 16-12-2016
 */
public class Constants {
    public static final int PER_PAGE = 20; // Number of course search results in a page
    // used for intent from CourseSearch to CourseDetailActivity for CourseEnrolFrag
    public static final String EXTRA_COURSE = "course_parcel";

    @Deprecated
    public static String API_URL = "";
    @Deprecated
    public static String TOKEN;

    public static String getCourseUrl(int courseId) {
        return API_URL + "course/view.php?id=" + courseId;
    }
}
