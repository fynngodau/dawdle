package godau.fynn.moodledirect.util;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteException;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.UiThread;
import androidx.annotation.WorkerThread;
import androidx.appcompat.app.AlertDialog;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.MoodleException;
import godau.fynn.moodledirect.module.FileManager;
import godau.fynn.moodledirect.network.exception.NotOkayException;
import godau.fynn.moodledirect.view.dialog.MoodleExceptionDialog;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.UnknownHostException;
import java.util.function.Consumer;

import javax.net.ssl.SSLException;

public class ExceptionHandler {

    /**
     * Handles exceptions that occur during execution of {@code runnable}. In case of exception, displays
     * message to user on main thread. In case of success, passes
     * result to {@code resultHandler}.
     */
    @WorkerThread
    public static <T> void tryAndThen(ExceptionableSupplier<T> runnable, Consumer<T> resultHandler, Context context) {
        tryAndThen(runnable, resultHandler, null, context);
    }

    /**
     * Handles exceptions that occur during execution of {@code runnable}. In case of exception, displays
     * message to user on main thread and also invokes {@code alsoOnFailure}. In case of success, passes
     * result to {@code resultHandler} on main thread.
     */
    @WorkerThread
    public static <T> void tryAndThen(ExceptionableSupplier<T> runnable, Consumer<T> resultHandler, @Nullable Consumer<Exception> alsoOnFailure, Context context) {
        try {
            T t = runnable.run();

            new Handler(Looper.getMainLooper()).post(() -> resultHandler.accept(t));

        } catch (MoodleException e) {
            e.printStackTrace();
            new Handler(Looper.getMainLooper()).post(() -> {

                if (alsoOnFailure != null) alsoOnFailure.accept(e);

                if (e.getErrorCode() == MoodleException.ErrorCode.INVALID_TOKEN) {
                    new AlertDialog.Builder(context, R.style.LoginTheme_AlertDialog)
                            .setTitle(R.string.moodle_exception_invalid_token)
                            .setMessage(R.string.moodle_exception_invalid_token_message)
                            .setPositiveButton(R.string._continue,
                                    (dialog, which) -> UserUtils.logoutAndFinishAffinity(context)
                            ).setCancelable(false).show();
                } else if (e.getErrorCode() == MoodleException.ErrorCode.INVALID_PRIVATE_TOKEN) {
                    Toast.makeText(context, R.string.moodle_exception_invalid_private_token, Toast.LENGTH_LONG).show();
                } else if (e.getErrorCode() == MoodleException.ErrorCode.AUTO_LOGIN_TIME_LIMITED) {
                    Toast.makeText(context, R.string.moodle_exception_auto_login_time_limited, Toast.LENGTH_LONG).show();
                    // Fix: update auto login cooldown
                    ConfigDownloadHelper.updateAutoLoginCooldown(context);
                } else {
                    new MoodleExceptionDialog(e, context).show();
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            // Suppress. Users should be informed that the operation will (likely) not succeed ahead of time.

            new Handler(Looper.getMainLooper()).post(() -> {
                if (alsoOnFailure != null) alsoOnFailure.accept(e);
            });

        } catch (IOException | JsonSyntaxException e) {

            @StringRes int errorMessage;
            if (e instanceof IOException) errorMessage = networkErrorMessage((IOException) e);
            else errorMessage = R.string.error_network_json;

            e.printStackTrace();
            new Handler(Looper.getMainLooper()).post(() -> {
                if (alsoOnFailure != null) alsoOnFailure.accept(e);
                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            });
        } catch (SQLiteException e) {
            e.printStackTrace();
            if (e.getMessage() != null && e.getMessage().contains("too many SQL variables")) {
                new Handler(Looper.getMainLooper()).post(() -> {
                    if (alsoOnFailure != null) alsoOnFailure.accept(e);
                    Toast.makeText(context, R.string.error_sqlite_too_many_variables, Toast.LENGTH_SHORT).show();
                });
            } else {
                new Handler(Looper.getMainLooper()).post(() -> {
                    new AlertDialog.Builder(context)
                            .setTitle(R.string.error_sqlite_other)
                            .setMessage(context.getString(R.string.error_sqlite_other_message, e.getMessage()))
                            .show();

                    if (alsoOnFailure != null) alsoOnFailure.accept(e);
                });
            }
        } catch (OfflineException e) {
            new Handler(Looper.getMainLooper()).post(() -> {
                if (alsoOnFailure != null) alsoOnFailure.accept(e);
                if (e.getCauseMessage() != Resources.ID_NULL) new AlertDialog.Builder(context)
                        .setMessage(e.getCauseMessage())
                        .setCancelable(true)
                        .show();
            });
        } catch (FileManager.DirectoryCreationException e) {
            new Handler(Looper.getMainLooper()).post(() -> {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.error_directory_creation)
                        .setMessage(context.getString(R.string.error_directory_creation_message))
                        .show();

                if (alsoOnFailure != null) alsoOnFailure.accept(e);
            });
        } catch (FileManager.FileCreationException e) {
            new Handler(Looper.getMainLooper()).post(() -> {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.error_file_creation)
                        .setMessage(context.getString(R.string.error_file_creation_message))
                        .show();

                if (alsoOnFailure != null) alsoOnFailure.accept(e);
            });
        }
    }

    public static @StringRes int networkErrorMessage(IOException e) {
        if (e instanceof ConnectException) return R.string.error_network_connect;
        else if (e instanceof NoRouteToHostException) return R.string.error_network_no_route;
        else if (e instanceof UnknownHostException) return R.string.error_network_unknown_host;
        else if (e instanceof InterruptedIOException) return R.string.error_network_timeout;
        else if (e instanceof SSLException) return R.string.error_network_ssl;
        else return R.string.error_network;
    }

    /**
     * Same as {@link #tryAndThen(ExceptionableSupplier, Consumer, Consumer, Context)}, but performs the
     * whole operation in a new thread.
     *
     * @return The thread that was created and started.
     */
    @UiThread
    public static <T> Thread tryAndThenThread(ExceptionableSupplier<T> runnable, Consumer<T> resultHandler, @Nullable Consumer<Exception> alsoOnFailure, Context context) {
        Thread thread = new Thread(() -> tryAndThen(runnable, resultHandler, alsoOnFailure, context));
        thread.start();
        return thread;
    }

    /**
     * Same as {@link #tryAndThen(ExceptionableSupplier, Consumer, Context)}, but performs the
     * whole operation in a new thread.
     *
     * @return The thread that was created and started.
     */
    @UiThread
    public static <T> Thread tryAndThenThread(ExceptionableSupplier<T> runnable, Consumer<T> resultHandler, Context context) {
        Thread thread = new Thread(() -> tryAndThen(runnable, resultHandler, context));
        thread.start();
        return thread;
    }

    public interface ExceptionableSupplier<T> {
        public T run() throws MoodleException, NotOkayException, JsonParseException, IOException, OfflineException;
    }

}
