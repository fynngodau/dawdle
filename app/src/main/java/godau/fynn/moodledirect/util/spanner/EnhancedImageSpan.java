package godau.fynn.moodledirect.util.spanner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.style.ImageSpan;
import androidx.annotation.NonNull;

public class EnhancedImageSpan extends ImageSpan {

    private int width, height;

    public EnhancedImageSpan(@NonNull Bitmap b) {
        super(b);
    }

    public EnhancedImageSpan(@NonNull Bitmap b, int verticalAlignment) {
        super(b, verticalAlignment);
    }

    public EnhancedImageSpan(@NonNull Context context, @NonNull Bitmap bitmap) {
        super(context, bitmap);
    }

    public EnhancedImageSpan(@NonNull Context context, @NonNull Bitmap bitmap, int verticalAlignment) {
        super(context, bitmap, verticalAlignment);
    }

    public EnhancedImageSpan(@NonNull Drawable drawable) {
        super(drawable);
    }

    public EnhancedImageSpan(@NonNull Drawable drawable, int verticalAlignment) {
        super(drawable, verticalAlignment);
    }

    public EnhancedImageSpan(@NonNull Drawable drawable, @NonNull String source) {
        super(drawable, source);
    }

    public EnhancedImageSpan(@NonNull Drawable drawable, @NonNull String source, int verticalAlignment) {
        super(drawable, source, verticalAlignment);
    }

    public EnhancedImageSpan(@NonNull Context context, @NonNull Uri uri) {
        super(context, uri);
    }

    public EnhancedImageSpan(@NonNull Context context, @NonNull Uri uri, int verticalAlignment) {
        super(context, uri, verticalAlignment);
    }

    public EnhancedImageSpan(@NonNull Context context, int resourceId) {
        super(context, resourceId);
    }

    public EnhancedImageSpan(@NonNull Context context, int resourceId, int verticalAlignment) {
        super(context, resourceId, verticalAlignment);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
