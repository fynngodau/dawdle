package godau.fynn.moodledirect.model.api.assign;

import androidx.annotation.StringRes;
import com.google.gson.annotations.SerializedName;
import godau.fynn.moodledirect.R;

public class Attempt {
    @SerializedName("gradingstatus")
    public String gradingStatus;

    public Submission submission;

    public enum Status {
        NOT_MARKED(R.string.assignment_grading_not_marked),
        IN_MARKING(R.string.assignment_grading_in_marking),
        READY_FOR_REVIEW(R.string.assignment_grading_ready_for_review),
        IN_REVIEW(R.string.assignment_grading_in_review),
        READY_FOR_RELEASE(R.string.assignment_grading_ready_for_release),
        RELEASED(R.string.assignment_grading_released);

        public final @StringRes int string;

        Status(@StringRes int string) {
            this.string = string;
        }
    }

    public Status getGradingStatus() {
        switch (gradingStatus) {
            case "notgraded":
            case "notmarked":
                return Status.NOT_MARKED;
            case "inmarking":
                return Status.IN_MARKING;
            case "readyforreview":
                return Status.READY_FOR_REVIEW;
            case "inreview":
                return Status.IN_REVIEW;
            case "readyforrelease":
                return Status.READY_FOR_RELEASE;
            case "released":
            case "graded":
                return Status.RELEASED;

            default: return null;
        }
    }


}
