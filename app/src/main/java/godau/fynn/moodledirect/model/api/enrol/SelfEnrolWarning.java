package godau.fynn.moodledirect.model.api.enrol;

/**
 * Created by siddhant on 12/19/16.
 */

public class SelfEnrolWarning {

    private final String item;
    private final int itemid;
    private final String warningcode;
    private final String message;

    public SelfEnrolWarning(String item, int itemid, String warningcode, String message) {
        this.item = item;
        this.itemid = itemid;
        this.warningcode = warningcode;
        this.message = message;
    }
}
