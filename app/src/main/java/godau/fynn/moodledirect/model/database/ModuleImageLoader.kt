package godau.fynn.moodledirect.model.database

import android.content.Context
import android.util.Log
import android.view.View
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.CachePolicy
import coil.request.ImageRequest
import godau.fynn.moodledirect.R
import godau.fynn.moodledirect.util.MyApplication
import godau.fynn.moodledirect.view.adapter.course.ModuleHandler.ModuleViewHolder

fun loadImage(holder: ModuleViewHolder, context: Context, url: String, offline: Boolean) {
    val imageLoader = ImageLoader.Builder(context)
        .components { add(SvgDecoder.Factory()) }.build()

    ImageRequest.Builder(context)
        .placeholder(R.drawable.ic_file_generic)
        .error(R.drawable.ic_file_generic)
        .data(url)
        .networkCachePolicy(if (offline) CachePolicy.DISABLED else CachePolicy.ENABLED)
        .target(onError = {
            Log.w("Module image", "Couldn't download module icon from $url")
            holder.progressBar.animate().alpha(0f)
            holder.icon.visibility = View.VISIBLE
        }, onSuccess = {
            holder.progressBar.animate().alpha(0f)
            holder.icon.visibility = View.VISIBLE
            holder.icon.setImageDrawable(it)
            if (MyApplication.getInstance().isDarkModeEnabled) {
                holder.colorBackground.visibility = View.VISIBLE
                holder.icon.scaleX = 0.7f
                holder.icon.scaleY = 0.7f
            } else {
                holder.icon.scaleX = 1f
                holder.icon.scaleY = 1f
                holder.colorBackground.visibility = View.GONE
            }

        }).build().let { imageLoader.enqueue(it) }
}