package godau.fynn.moodledirect.model.api.enrol;

import com.google.gson.annotations.SerializedName;

public class SelfEnrolmentOption extends EnrolmentOption {
    @SerializedName("enrolpassword")
    public String enrollmentPasswordHint;

}
