package godau.fynn.moodledirect.model.database.file;

import android.net.Uri;
import androidx.room.Entity;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;
import godau.fynn.moodledirect.model.api.file.File;

@Entity(tableName = "downloadedFile")
public class DownloadedFile extends File {

    /**
     * Storage location of this file on the device
     */
    @TypeConverters(DownloadedFile.class)
    public Uri location;

    public DownloadedFile() {}

    public DownloadedFile(File file, Uri uri) {
        filename = file.filename;
        size = file.size;
        url = file.url;
        timeModified = file.timeModified;
        mimetype = file.mimetype;
        reference = file.reference;
        orderNumber = file.orderNumber;
        this.location = uri;
    }

    @TypeConverter
    public String fromUri(Uri uri) {
        return uri.toString();
    }

    @TypeConverter
    public Uri parseUri(String string) {
        return Uri.parse(string);
    }

}
