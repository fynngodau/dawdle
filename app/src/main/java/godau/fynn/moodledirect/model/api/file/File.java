package godau.fynn.moodledirect.model.api.file;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import godau.fynn.moodledirect.module.FileManager;

import java.io.Serializable;

@Entity(tableName = "file")
public class File implements Serializable {

    public String filename;
    @SerializedName("filesize")
    public int size;

    @PrimaryKey @NonNull
    @SerializedName(value = "fileurl", alternate = {"url"})
    public String url;

    @SerializedName("timemodified")
    public long timeModified;

    @SerializedName(value = "mimetype", alternate = {"type"})
    public String mimetype;

    // === DATABASE ===

    /**
     * Just number → module ID
     * "choice" + number → instance ID of choice module
     */
    public String reference;
    public int orderNumber;

    // === NOT IN DATABASE ===

    @Ignore
    public FileManager.DownloadStatus downloadStatus;

    // === DATABASE ASSEMBLER ===

}
