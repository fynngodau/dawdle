package godau.fynn.moodledirect.model.database;

import android.text.Html;

import androidx.room.Ignore;

import java.util.List;

import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.view.ExpandableTextDisplay;

/**
 * Created by harsu on 17-12-2016.
 */

public class CourseSection extends Module {

    public CourseSection() {}

    // === DOWNLOAD ===
    @Ignore
    public List<Module> modules;

    // === DATABASE EXTRACTION ===
    public CourseSection(Module module) {
        super();
        name = module.name;
        description = module.description;
        available = module.available;
        notAvailableReason = module.notAvailableReason;
    }

    @Override
    public ResourceType getModuleType() {
        return ResourceType.UNKNOWN;
    }

    // === LEGACY ===

    public void setId(int id) {
        this.id = id;
    }

    public void setSummary(String summary) {
        description = summary;
    }

    public String getSummary() {
        return description;
    }

    public int getId() {
        return id;
    }

    public List<Module> getModules() {
        return modules;
    }

    @Override
    public boolean equals(Object obj) {

        return obj instanceof CourseSection && ((CourseSection) obj).id == id;
    }
}
