package godau.fynn.moodledirect.model.api.base;

import java.util.List;

import godau.fynn.moodledirect.model.database.Course;

/**
 * Only for {@code Core.getCourseById} and {@code MoodleServices.searchCourses}!
 */
public class CourseList {
    public List<Course> courses;
}
