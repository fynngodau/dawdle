package godau.fynn.moodledirect.model.api.forum;

public class AuthorData {
    public int id;
    public String fullname;
    public AvatarData urls;
}
