package godau.fynn.moodledirect.model.api.base;

public abstract class ServiceResponse<T> {
    public T data;

    public static class PublicConfig extends ServiceResponse<godau.fynn.moodledirect.model.api.base.PublicConfig> {}

    public static class UserToken extends ServiceResponse<godau.fynn.moodledirect.model.api.UserToken> {}
}
