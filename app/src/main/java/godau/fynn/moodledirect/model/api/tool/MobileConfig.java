package godau.fynn.moodledirect.model.api.tool;

import java.util.List;

public class MobileConfig {

    public List<Pair> settings;

    public static class Pair {
        public String name, value;
    }
}
