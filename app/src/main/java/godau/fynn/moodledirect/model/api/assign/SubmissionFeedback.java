package godau.fynn.moodledirect.model.api.assign;

import com.google.gson.annotations.SerializedName;

public class SubmissionFeedback {
    @SerializedName("gradefordisplay")
    public String gradeText;
}
