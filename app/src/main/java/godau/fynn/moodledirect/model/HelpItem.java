package godau.fynn.moodledirect.model;

import androidx.annotation.StringRes;
import godau.fynn.moodledirect.R;

public enum HelpItem {

    // Violating the norm, these constants are not in all caps for readability.
    welcome(R.string.help_welcome, R.string.help_welcome_content),
    author(R.string.help_author, R.string.help_author_content),
    project(R.string.help_project, R.string.help_project_content),
    privacy(R.string.help_privacy, R.string.help_privacy_content),
    instance(R.string.help_instance, R.string.help_instance_content),
    rendering(R.string.help_rendering, R.string.help_rendering_content),
    files(R.string.help_files, R.string.help_files_content),
    offline(R.string.help_offline, R.string.help_offline_content),
    enrolment(R.string.help_enrolment, R.string.help_enrolment_content),
    completion(R.string.help_completion, R.string.help_completion_content),
    choiceResults(R.string.help_choice_results, R.string.help_choice_results_content),
    autoLogin(R.string.settings_web_auto_login, R.string.help_auto_login_content),
    contribute(R.string.help_contribute, R.string.help_contribute_content),
    about(R.string.help_about, Type.ABOUT),
    imprint(R.string.help_imprint, R.string.help_imprint_content),
    ;

    public final @StringRes int title, content;
    public final Type type;

    HelpItem(@StringRes int title, @StringRes int content) {
        this.title = title;
        this.content = content;
        this.type = Type.TEXT;
    }

    HelpItem(@StringRes int title, Type type) {
        assert type != Type.TEXT;
        this.type = type;
        this.title = title;
        content = 0;
    }

    public enum Type {
        TEXT, ABOUT
    }
}
