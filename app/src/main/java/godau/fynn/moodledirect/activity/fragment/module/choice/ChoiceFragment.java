package godau.fynn.moodledirect.activity.fragment.module.choice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.help.HelpActivity;
import godau.fynn.moodledirect.model.HelpItem;
import godau.fynn.moodledirect.module.Choice;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.NoDataView;

/**
 * Displays {@link ChoiceChooseFragment} and {@link ChoiceResultFragment}
 */
public class ChoiceFragment extends Fragment {

    private WeakReference<ChoiceResultFragment> resultFragment = new WeakReference<>(null);

    private int choiceId, courseId;

    private ViewPager2 viewPager;
    private TabLayout tabLayout;

    private Optional<ChoiceObjectThread> getChoiceObjectThread = Optional.empty();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        choiceId = getArguments().getInt(ChoiceFragmentFactory.EXTRA_CHOICE_ID);
        courseId = getArguments().getInt(ChoiceFragmentFactory.EXTRA_COURSE_ID);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choice, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        tabLayout = view.findViewById(R.id.tabs);
        viewPager = view.findViewById(R.id.view_pager);

        viewPager.setAdapter(new FragmentStateAdapter(this) {
            @NonNull
            @Override
            public Fragment createFragment(int position) {
                switch (position) {
                    case 0:
                        return ChoiceFragmentFactory.makeChoiceChooseFragment(choiceId, courseId);
                    case 1:
                        return ChoiceFragmentFactory.makeChoiceResultFragment(choiceId, courseId);
                }
                throw new IllegalArgumentException();
            }

            @Override
            public int getItemCount() {
                return 2;
            }
        });

        viewPager.setUserInputEnabled(false);

        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
            if (position == 0) tab.setText(R.string.choice);
            else tab.setText(R.string.choice_title_results);
        }).attach();

        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                setHasOptionsMenu(position == 1);
                if (position == 1) {
                    Activity activity = requireActivity();
                    if (activity instanceof NoDataView.InvokeListener) {
                        ((NoDataView.InvokeListener) activity).onInvokeNoData();
                    }
                }
            }
        });
    }

    void setResultFragment(ChoiceResultFragment fragment) {
        resultFragment = new WeakReference<>(fragment);
    }

    /**
     * Tells results tab to load data, and updates.
     */
    void notifyResultsChanged() {
        if (resultFragment.get() != null) resultFragment.get().loadData(MyApplication.moodle().getDispatch());
        viewPager.setCurrentItem(1, true);
    }

    @WorkerThread
    Choice.ChoiceObject getChoiceObject() throws IOException {
        // Join existing download thread, create new one otherwise
        ChoiceObjectThread thread;
        synchronized (getChoiceObjectThread) {
            if (getChoiceObjectThread.isPresent()) {
                Log.d(ChoiceFragment.class.getSimpleName(), "Joining existing download thread");
                thread = getChoiceObjectThread.get();
            } else {
                Log.d(ChoiceFragment.class.getSimpleName(), "Creating new download thread");
                thread = new ChoiceObjectThread();
                thread.start();
                getChoiceObjectThread = Optional.of(thread);
            }
        }
        try {
            thread.join();
            return thread.getResult();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new IOException(e);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_choice_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.help) {
            Intent intent = new Intent(requireActivity(), HelpActivity.class);
            intent.putExtra(HelpActivity.EXTRA_HELP_ITEM, HelpItem.choiceResults);
            startActivity(intent);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private class ChoiceObjectThread extends Thread {

        private IOException e;
        private Choice.ChoiceObject result;

        @Override
        public void run() {
            try {
                result = MyApplication.moodle().getDispatch().getChoice().getChoiceObject(choiceId, courseId);
            } catch (Exception e) {
                // Buffer this exception
                this.e = new IOException(e);
            }

            // Cleanup reference to our thread (and thereby our result) with delay
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    synchronized (getChoiceObjectThread) {
                        getChoiceObjectThread = Optional.empty();
                    }
                }
            }, 200);
        }

        Choice.ChoiceObject getResult() throws IOException {
            if (e != null) throw e;
            return result;
        }
    }
}
