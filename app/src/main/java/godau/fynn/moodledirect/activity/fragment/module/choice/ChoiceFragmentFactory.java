package godau.fynn.moodledirect.activity.fragment.module.choice;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class ChoiceFragmentFactory {

    static final String EXTRA_CHOICE_ID = "choiceId";
    static final String EXTRA_COURSE_ID = "courseId";

    public static ChoiceFragment makeChoiceFragment(int choiceId, int courseId) {
        return putBundle(new ChoiceFragment(), choiceId, courseId);
    }

    static ChoiceChooseFragment makeChoiceChooseFragment(int choiceId, int courseId) {
        return putBundle(new ChoiceChooseFragment(), choiceId, courseId);
    }

    static ChoiceResultFragment makeChoiceResultFragment(int choiceId, int courseId) {
        return putBundle(new ChoiceResultFragment(), choiceId, courseId);
    }

    private static <T extends Fragment> T putBundle(T fragment, int choiceId, int courseId) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_CHOICE_ID, choiceId);
        bundle.putInt(EXTRA_COURSE_ID, courseId);
        fragment.setArguments(bundle);
        return fragment;
    }
}
