package godau.fynn.moodledirect.activity.fragment.module.choice;

import static godau.fynn.moodledirect.model.api.choice.ChoiceDetails.RESULT_AVAILABILITY_AFTER_ANSWER;
import static godau.fynn.moodledirect.model.api.choice.ChoiceDetails.RESULT_AVAILABILITY_AFTER_CLOSE;
import static godau.fynn.moodledirect.model.api.choice.ChoiceDetails.RESULT_AVAILABILITY_ALWAYS;
import static godau.fynn.moodledirect.model.api.choice.ChoiceDetails.RESULT_AVAILABILITY_NEVER;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.viewpager2.widget.ViewPager2;

import java.util.Optional;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.SwipeRefreshFragment;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.api.choice.ChoiceOption;
import godau.fynn.moodledirect.model.api.choice.ChoiceResult;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.adapter.ChoiceResultAdapter;
import im.dacer.androidcharts.bar.BarView;
import im.dacer.androidcharts.bar.MultiValue;
import im.dacer.androidcharts.bar.Value;

public class ChoiceResultFragment extends SwipeRefreshFragment {

    ViewGroup rootView;
    private BarView barView;
    private View divider;
    private ViewPager2 viewPager;

    private Context context;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((ChoiceFragment) getParentFragment()).setResultFragment(this);
    }

    @Override
    protected View onCreateContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choice_results, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rootView = view.findViewById(R.id.root);
        barView = view.findViewById(R.id.bar_view);
        divider = view.findViewById(R.id.divider);
        viewPager = view.findViewById(R.id.view_pager);
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {
        Context context = requireContext();
        ExceptionHandler.tryAndThenThread(
                // Ignore given dispatch; choice should be updated even if restoring instance state
                () -> ((ChoiceFragment) getParentFragment()).getChoiceObject(),
                choice -> {

                    // Determine whether results should be available

                    boolean resultsAvailable = false;
                    @StringRes int errorMessage;

                    switch (choice.details.resultAvailability) {
                        default:
                        case RESULT_AVAILABILITY_NEVER:
                            resultsAvailable = false;
                            errorMessage = R.string.choice_results_not_available_never;
                            break;
                        case RESULT_AVAILABILITY_AFTER_ANSWER:

                            for (ChoiceOption option : choice.options) {
                                if (option.checked) {
                                    resultsAvailable = true;
                                    break;
                                }
                            }

                            errorMessage = R.string.choice_results_not_available_after_answer;
                            break;

                        case RESULT_AVAILABILITY_AFTER_CLOSE:
                            resultsAvailable = choice.details.timeClose > 0 &&
                                    choice.details.timeClose <= (System.currentTimeMillis() / 1000);
                            errorMessage = R.string.choice_results_not_available_after_close;
                            break;

                        case RESULT_AVAILABILITY_ALWAYS:
                            resultsAvailable = true;
                            errorMessage = 0;
                            break;

                    }

                    if (!resultsAvailable) {
                        empty.text(errorMessage, R.drawable.eye_off);
                        rootView.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                    } else ExceptionHandler.tryAndThenThread(
                            // Ignore given dispatch again for live results, and because offline storage does not contain onymous choices
                            () -> MyApplication.moodle().getDispatch().getChoice().getChoiceResults(
                                    getArguments().getInt(ChoiceFragmentFactory.EXTRA_CHOICE_ID)
                            ),
                            results -> {
                                rootView.setVisibility(View.VISIBLE);
                                swipeRefreshLayout.setRefreshing(false);
                                empty.hide();

                                Value[] barViewData = results.stream()
                                        .map(result -> {
                                            int remaining = result.answerLimit - result.answerCount;
                                            if (remaining > 0) {
                                                return new MultiValue(
                                                        new int[]{result.answerCount, remaining},
                                                        new Integer[]{
                                                                null,
                                                                context.getResources().getColor(R.color.gray_alpha50)
                                                        }, result.text
                                                );
                                            } else {
                                                return new Value(result.answerCount, result.text);
                                            }
                                        })
                                        .toArray(Value[]::new);

                                /* Calculate total number of users by percentage amount of any option
                                 * with more than zero users. The accuracy will be high enough due to
                                 * the many decimal digits that the percentage value provides. If no
                                 * bar has more than zero, set maximum to 1.
                                 */
                                Optional<ChoiceResult> nonZeroResult = results.stream()
                                        .filter(result -> result.answerCount > 0)
                                        .findAny();

                                if (nonZeroResult.isPresent()) {
                                    int max = (int) Math.round(
                                            nonZeroResult.get().answerCount /
                                                    (nonZeroResult.get().answerPercentage / 100)
                                    );
                                    barView.setData(
                                            barViewData, max
                                    );
                                } else {
                                    barView.setData(barViewData, 1);
                                }

                                if (
                                        choice.details.resultOnymous &&
                                                results.stream().anyMatch(result -> result.users.size() > 0)
                                ) {
                                    viewPager.setVisibility(View.VISIBLE);
                                    viewPager.setAdapter(new ChoiceResultAdapter(results));

                                    divider.setVisibility(View.VISIBLE);

                                    barView.setShowCursor(true);
                                    barView.setOnBarClickListener(position -> viewPager.setCurrentItem(position, true));
                                    viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                                        @Override
                                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                            barView.setCursorPosition(position, positionOffset);
                                        }
                                    });

                                } else {
                                    viewPager.setVisibility(View.GONE);
                                    divider.setVisibility(View.GONE);
                                    barView.setShowCursor(false);
                                    barView.setOnBarClickListener(null);
                                }

                            },
                            this::onFailure,
                            context
                    );
                },
                this::onFailure,
                context
        );
    }

    private void onFailure(Exception e) {
        empty.exception(e);
        swipeRefreshLayout.setRefreshing(false);
        rootView.setVisibility(View.GONE);
    }
}
