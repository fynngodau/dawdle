package godau.fynn.moodledirect.activity.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.CourseDetailActivity;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.module.basic.Core;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.ConfigDownloadHelper;
import godau.fynn.moodledirect.view.adapter.CoursesAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class EnrolledCoursesFragment extends SwipeRefreshFragment {

    private List<Course> courses;

    private String searchText = "";

    private CoursesAdapter adapter;

    //private int coursesUpdated;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        adapter = new CoursesAdapter(courses = new ArrayList<>(), new PreferenceHelper(requireContext()).getCourseRowAppearance());
        adapter.setStateRestorationPolicy(RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY);
        adapter.setClickListener((course, position) -> {
            Intent intent = new Intent(getActivity(), CourseDetailActivity.class);
            intent.putExtra("id", course.id);
            intent.putExtra("course_name", course.shortname);
            startActivity(intent);
        });
    }

    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, ViewGroup container,
                                    Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_enrolled_courses, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        empty = view.findViewById(R.id.empty);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {
        Core coreModule = dispatch.getCore();
        
        ExceptionHandler.tryAndThenThread(
                () -> courses = coreModule.getCourses(),
                courses -> {
                    adapter.setCourses(courses);

                    if (courses.isEmpty()) {
                        empty.text(R.string.empty_courses);
                    } else {
                        empty.hide();
                    }

                    filterCourses(searchText);
                    swipeRefreshLayout.setRefreshing(false);
                },
                e -> {
                    swipeRefreshLayout.setRefreshing(false);

                    empty.exception(e);

                    adapter.setCourses(Collections.emptyList());

                },
                requireContext()
        );

        // Also update mobile config
        ConfigDownloadHelper.updateSiteInformation(dispatch, requireContext());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my_courses_menu, menu);

        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchText = newText;
                filterCourses(newText);
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void filterCourses(String searchedText) {
        if (searchedText.isEmpty()) {
            adapter.setCourses(courses);

        } else {
            searchedText = searchText.toLowerCase();
            List<Course> filteredCourses = new ArrayList<>();
            courses:
            for (Course course : courses) {
                for (String toSearch : new String[]{
                        course.name, course.shortname, course.topCategoryName,
                        course.categoryName, course.summary
                }) if (toSearch != null && toSearch.toLowerCase().contains(searchedText)) {
                    filteredCourses.add(course);
                    continue courses;
                }
            }
            adapter.setCourses(filteredCourses);
        }
    }
}
