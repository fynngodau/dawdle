package godau.fynn.moodledirect.activity.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.view.*;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.help.HelpActivity;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.HelpItem;
import godau.fynn.moodledirect.model.api.enrol.EnrolmentOption;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.module.basic.Enrolment;
import godau.fynn.moodledirect.util.*;
import godau.fynn.moodledirect.view.EnrolmentOptionsViewFactory;
import godau.fynn.moodledirect.view.ImageLoaderTextView;
import godau.fynn.moodledirect.view.NoDataView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CourseEnrolFragment extends SwipeRefreshFragment {

    private static final String COURSE_KEY = "course";

    private Course course;

    private List<EnrolmentOption> enrolmentOptions = new ArrayList<>();

    public static CourseEnrolFragment newInstance(Course course) {
        CourseEnrolFragment fragment = new CourseEnrolFragment();
        Bundle args = new Bundle();
        args.putSerializable(COURSE_KEY, course);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            course = (Course) getArguments().getSerializable(COURSE_KEY);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_course_enrol, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ImageLoaderTextView description = view.findViewById(R.id.description);
        if (course.summary.trim().isEmpty()) {
            description.setVisibility(View.GONE);
        } else {
            description.setVisibility(View.VISIBLE);

            SpannableStringBuilder renderedText = TextUtil.fromHtml(course.summary, requireContext(), description.getWidth());
            description.setText(renderedText,
                    Collections.emptyList(), MyApplication.moodle().getDispatch().getCommonAsset(),
                    getResources().getDisplayMetrics()
            );
            description.setTextIsSelectable(true);
            description.setMovementMethod(LinkMovementMethod.getInstance());
        }

        // Load "already enrolled" message box
        ExceptionHandler.tryAndThenThread(
                () -> MyApplication.moodle().forceOffline().getCore().isEnrolledTo(course.id),
                isEnrolled -> {
                    if (isEnrolled) {
                        view.findViewById(R.id.already_enrolled).setVisibility(View.VISIBLE);

                        view.findViewById(R.id.already_enrolled_open).setOnClickListener(v ->
                                // Don't add to back stack; unlikely that users want to go back to enrollment options
                                requireActivity().getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.course_activity_frame,
                                                CourseFragment.newInstance(course), null)
                                        .commit()
                        );
                    } else {
                        view.findViewById(R.id.already_enrolled).setVisibility(View.GONE);
                    }
                },
                requireContext()
        );

        produceViews(view, enrolmentOptions);
    }

    private void produceViews(View view, List<EnrolmentOption> methods) {
        LinearLayout enrolmentFrame = view.findViewById(R.id.enrolment_frame);
        enrolmentFrame.removeAllViews();

        EnrolmentOptionsViewFactory.addEnrolmentOptions(
                enrolmentFrame, methods,
                () -> { // on enrol

                    // Show course now without adding transaction to back stack
                    requireActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.course_activity_frame,
                                    CourseFragment.newInstance(course), null)
                            .commit();

                    // Add to own course list
                    ExceptionHandler.tryAndThenThread(
                            () -> MyApplication.moodle().getDispatch().getCore().getCourse(course.id),
                            then -> {},
                            requireContext()
                    );
                }, () -> { // on guest enrol
                    // Show course, but allow navigating back
                    requireActivity().getSupportFragmentManager().beginTransaction()
                            .addToBackStack(null)
                            .replace(R.id.course_activity_frame,
                                    CourseFragment.newInstance(course, true), null)
                            .commit();

                }
        );
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {

        NoDataView empty = requireView().findViewById(R.id.enrolment_empty);

        Context downloadContext = requireContext();

        // Load enrollment options (ignore recommended dispatch)
        ExceptionHandler.tryAndThenThread(
                () -> MyApplication.moodle().getDispatch().getEnrolment().getEnrolmentMethods(course.id),
                methods -> {
                    swipeRefreshLayout.setRefreshing(false);

                    if (methods.isEmpty()) {
                        empty.text(R.string.empty_course_enrol);
                    } else {
                        empty.hide();
                        produceViews(requireView(), enrolmentOptions = methods);
                    }
                },
                onFailure -> {
                    swipeRefreshLayout.setRefreshing(false);
                    empty.exception(onFailure);
                },
                downloadContext
        );
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.course_enrol_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_open_in_browser) {
            AutoLoginHelper.openWithAutoLogin(getActivity(), getView(), Constants.getCourseUrl(course.id));
            return true;
        } else if (item.getItemId() == R.id.action_help) {
            Intent intent = new Intent(requireActivity(), HelpActivity.class);
            intent.putExtra(HelpActivity.EXTRA_HELP_ITEM, HelpItem.enrolment);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
