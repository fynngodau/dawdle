package godau.fynn.moodledirect.activity.help.fragment;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.HelpItem;

public class HelpTextFragment extends Fragment {

    public static final String EXTRA_ITEM = "item";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.page_help_text, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TextView textView = view.findViewById(R.id.help_text);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(((HelpItem) getArguments().getSerializable(EXTRA_ITEM)).content);

    }
}
