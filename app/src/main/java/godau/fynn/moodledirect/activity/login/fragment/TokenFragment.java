package godau.fynn.moodledirect.activity.login.fragment;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.login.LoginActivity;
import godau.fynn.moodledirect.model.api.UserToken;
import godau.fynn.moodledirect.model.api.base.PublicConfig;
import godau.fynn.moodledirect.module.basic.Login;
import godau.fynn.moodledirect.util.ScrollableFragment;

import java.util.concurrent.atomic.AtomicReference;

public class TokenFragment extends ScrollableFragment {

    private static final String EXTRA_CONFIG = "config";

    public static TokenFragment get(PublicConfig config) {
        TokenFragment fragment = new TokenFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_CONFIG, config);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ScrollView container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_token, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextInputEditText tokenView = view.findViewById(R.id.token);
        Button login = view.findViewById(R.id.login);

        tokenView.setOnKeyListener((v, i, keyEvent) -> {
                    if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                        login.performClick();
                        return true;
                    } else return false;
                }
        );

        tokenView.requestFocus();

        login.setOnClickListener(v -> {
            Uri tokenUri = Uri.parse(
                    tokenView.getEditableText().toString()
                    .replace("://token", "://?token")
            );

            String encodedToken;
            if (tokenUri.getQueryParameterNames().contains("token")) {
                Log.d(TokenFragment.class.getSimpleName(), "Discovered that token was passed as a query parameter, using that");
                encodedToken = tokenUri.getQueryParameter("token");
            } else {
                Log.d(TokenFragment.class.getSimpleName(), "Interpreting entire value as token");
                encodedToken = tokenView.getEditableText().toString();
            }

            // Prepare String to just get base64
            String[] splitDecodedToken;
            try {
                byte[] decodeValue = Base64.decode(encodedToken, Base64.DEFAULT);
                String decodedToken = new String(decodeValue);
                splitDecodedToken = decodedToken.split(":::");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                splitDecodedToken = new String[0];
            }

            UserToken token = new UserToken();
            if (splitDecodedToken.length < 2) {
                Log.d(TokenFragment.class.getSimpleName(), "Base64 decoding did not yield expected result; " +
                        "treating token as already decoded token for login (not setting private token)");
                token.token = encodedToken;
            } else {
                Log.d(TokenFragment.class.getSimpleName(), "Token gathered from decoded Base64");
                token.token = splitDecodedToken[1];

                if (splitDecodedToken.length >= 3) {
                    token.privatetoken = splitDecodedToken[2];
                }
            }


            AtomicReference<Thread> thread = new AtomicReference<>();

            ProgressDialog progressDialog = new ProgressDialog(requireActivity(), R.style.LoginTheme_AlertDialog);

            // Configure cancellable dialog
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(dialog -> {
                thread.get().interrupt();
            });

            progressDialog.show();

            thread.set(((LoginActivity) requireActivity()).advanceLogin(
                    new Login(requireActivity()),
                    token,
                    progressDialog,
                    (PublicConfig) getArguments().getSerializable(EXTRA_CONFIG),
                    null
            ));
        });
    }
}
