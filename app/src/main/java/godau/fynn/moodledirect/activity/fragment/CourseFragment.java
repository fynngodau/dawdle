package godau.fynn.moodledirect.activity.fragment;

import android.os.Bundle;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SimpleItemAnimator;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.util.*;
import godau.fynn.moodledirect.view.adapter.course.ModuleAdapter;
import godau.fynn.moodledirect.view.adapter.course.ModuleDividerDecoration;

/**
 * Created by SKrPl on 12/21/16.
 */

public class CourseFragment extends SwipeRefreshFragment {

    private static final String KEY_COURSE = "course";

    private static final String KEY_GUEST = "guest";

    private int courseId;
    private boolean guest;
    private ModuleAdapter adapter;

    public static CourseFragment newInstance(Course course) {
        return newInstance(course, false);
    }

    public static CourseFragment newInstance(Course course, boolean guest) {
        CourseFragment courseFragment = new CourseFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_COURSE, course);
        args.putBoolean(KEY_GUEST, guest);
        courseFragment.setArguments(args);
        return courseFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();

        FileManagerWrapper fileManager = MyApplication.moodle().getDispatch().getFile().wrap();
        adapter = new ModuleAdapter(fileManager);

        if (args != null) {
            Course course = (Course) args.getSerializable(KEY_COURSE);
            guest = args.getBoolean(KEY_GUEST);

            adapter.setCourse(course);
            courseId = course.id;
        } else throw new IllegalArgumentException();

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_course, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter.setStateRestorationPolicy(RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY);
        recyclerView.setAdapter(adapter);

        // Enable smooth expanding text transitions
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        // Add dividers as decoration
        recyclerView.addItemDecoration(new ModuleDividerDecoration(getContext(), adapter, recyclerView.getLayoutManager()));
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {
        ExceptionHandler.tryAndThenThread(
                () -> dispatch.getCourseContent().getCourseModules(courseId),
                modules -> {
                    adapter.clear();
                    adapter.addModules(modules);
                    swipeRefreshLayout.setRefreshing(false);

                    if (modules.isEmpty()) {
                        empty.text(R.string.empty_course);
                    } else {
                        empty.hide();
                    }
                },
                failure -> {
                    swipeRefreshLayout.setRefreshing(false);

                    empty.exception(failure);

                    adapter.clear();

                },
                getContext()
        );

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(
                guest? R.menu.open_in_browser : R.menu.course_details_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_open_in_browser) {
            AutoLoginHelper.openWithAutoLogin(getActivity(), getView(), Constants.getCourseUrl(courseId));
            return true;
        } else if (item.getItemId() == R.id.action_information) {
            getParentFragmentManager().beginTransaction()
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .replace(
                            R.id.course_activity_frame,
                            CourseInformationFragment.newInstance(courseId),
                            null
                    )
                    .commit();
            return true;
        } else return super.onOptionsItemSelected(item);
    }
}
