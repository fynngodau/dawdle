package godau.fynn.moodledirect.activity.login.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.login.LoginActivity;
import godau.fynn.moodledirect.model.api.base.PublicConfig;
import godau.fynn.moodledirect.module.basic.Login;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.ScrollableFragment;

import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class CredentialsFragment extends ScrollableFragment {

    private static final String EXTRA_CONFIG = "config";

    private PublicConfig config;

    private boolean allowSubmit = true;

    public static CredentialsFragment get(PublicConfig config) {
        CredentialsFragment fragment = new CredentialsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_CONFIG, config);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        config = (PublicConfig) getArguments().getSerializable(EXTRA_CONFIG);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ScrollView container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_credentials, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        EditText usernameEditText = view.findViewById(R.id.username);
        EditText passwordEditText = view.findViewById(R.id.password);
        Button login = view.findViewById(R.id.login);

        // Prefill well-known test instances
        if (config.getHttpswwwroot().equals("https://school.moodledemo.net/")) {
            usernameEditText.setText("student");
            passwordEditText.setText("moodle");
            login.requestFocus();
        } else if (config.getHttpswwwroot().equals("https://sandbox.moodledemo.net/")) {
            usernameEditText.setText("student");
            passwordEditText.setText("sandbox");
            login.requestFocus();
        }

        passwordEditText.setOnKeyListener((v, i, keyEvent) -> {
                    if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                        login.performClick();
                        return true;
                    } else return false;
                }
        );

        usernameEditText.requestFocus();

        login.setOnClickListener(v -> {
            if (!allowSubmit) return;

            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();

            boolean error = false;

            if (username.length() < 1) {
                usernameEditText.setError(getString(R.string.login_invalid_empty_username));
                error = true;
            }
            if (password.length() < 1) {
                passwordEditText.setError(getString(R.string.login_invalid_empty_password));
                error = true;
            }
            if (error) return;


            AtomicReference<Thread> thread = new AtomicReference<>();

            ProgressDialog progressDialog = new ProgressDialog(requireContext(), R.style.LoginTheme_AlertDialog);

            // Common failure handler
            Consumer<Exception> onFailure = failure -> {
                allowSubmit = true;
                progressDialog.dismiss();
            };

            // Configure cancellable dialog
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(dialog -> {
                thread.get().interrupt();
                onFailure.accept(new InterruptedException("Interrupted by user"));
            });

            // Display dialog
            progressDialog.setMessage(getString(R.string.login_progress_authenticating));
            progressDialog.show();

            Login loginService = new Login(requireContext());

            allowSubmit = false;

            LoginActivity downloadContext = (LoginActivity) requireActivity();

            thread.set(ExceptionHandler.tryAndThenThread(
                    () -> loginService.basicAuth(config, username, password),
                    token -> thread.set(downloadContext.advanceLogin(
                            loginService, token, progressDialog, config, onFailure
                    )),
                    onFailure,
                    downloadContext
            ));
        });
    }
}
