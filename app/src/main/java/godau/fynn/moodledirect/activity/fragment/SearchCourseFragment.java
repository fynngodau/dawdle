package godau.fynn.moodledirect.activity.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import java.util.ArrayList;

import godau.fynn.moodledirect.activity.CourseDetailActivity;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.module.basic.Core;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.NoDataView;
import godau.fynn.moodledirect.view.adapter.CoursesAdapter;

import static godau.fynn.moodledirect.util.Constants.EXTRA_COURSE;

public class SearchCourseFragment extends Fragment {

    private EditText editText;
    private View searchButton;
    private CoursesAdapter courseAdapter;
    private String previousSearch = "";
    private SwipeRefreshLayout swipeRefreshLayout;
    private NoDataView empty;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_course, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        PreferenceHelper.CourseRowAppearance courseRowAppearance = new PreferenceHelper.CourseRowAppearance();
        courseRowAppearance.showDescription = true;
        courseRowAppearance.showHeaderImage = true;
        courseRowAppearance.categoryAppearance = "bottom";

        courseAdapter = new CoursesAdapter(new ArrayList<>(), courseRowAppearance);
        courseAdapter.setClickListener((course, position) -> {
            Intent intent = new Intent(getActivity(), CourseDetailActivity.class);
            intent.putExtra(EXTRA_COURSE, course);
            startActivity(intent);
        });

        RecyclerView recyclerView = view.findViewById(R.id.searched_courses);
        empty = view.findViewById(R.id.empty);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());

        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        editText = view.findViewById(R.id.course_search_edit_text);
        searchButton = view.findViewById(R.id.course_search_button);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(courseAdapter);
        courseAdapter.setStateRestorationPolicy(RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY);

        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        editText.requestFocus();
        ((InputMethodManager) requireContext().getSystemService(Context.INPUT_METHOD_SERVICE))
                .showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            courseAdapter.setCourses(new ArrayList<>());
            getSearchCourses(previousSearch);
        });

        editText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchButton.callOnClick();
                return true;
            } else return false;
        });

        editText.setOnKeyListener((v, i, keyEvent) -> {
                    if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                        searchButton.callOnClick();
                        return true;
                    } else return false;
                }
        );

        searchButton.setOnClickListener(v -> {
            String searchText = editText.getText().toString().trim();
            if (searchText.isEmpty()) {
                return;
            }

            swipeRefreshLayout.setRefreshing(true);
            previousSearch = searchText;
            courseAdapter.setCourses(new ArrayList<>());
            getSearchCourses(searchText);

            ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                    .hideSoftInputFromWindow(editText.getWindowToken(), 0);
        });
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        searchButton.callOnClick();
    }

    private void getSearchCourses(final String query) {
        empty.setVisibility(View.GONE);

        Core core = MyApplication.moodle().getDispatch().getCore();

        ExceptionHandler.tryAndThenThread(
                () -> core.searchCourses(query),
                courses -> {

                    if (courses.isEmpty()) empty.text(R.string.empty_search);
                    else {
                        courseAdapter.setCourses(courses);
                    }
                    swipeRefreshLayout.setRefreshing(false);
                },
                failure -> {
                    swipeRefreshLayout.setRefreshing(false);
                    empty.exception(failure);
                },
                requireContext()
        );
    }
}
