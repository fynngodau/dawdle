package godau.fynn.moodledirect.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.module.link.ModuleLink;
import godau.fynn.moodledirect.util.MyApplication;

public class SupportedModulesActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (MyApplication.getInstance().isDarkModeEnabled()) {
            setTheme(R.style.AppTheme_NoActionBar_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_supported_modules);

        setSupportActionBar(findViewById(R.id.toolbar));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        LinearLayout moduleLayout = findViewById(R.id.moduleLayout);

        UserAccount account = new PreferenceHelper(this).getUserAccount();

        LayoutInflater inflater = LayoutInflater.from(this);

        List<View> views = new LinkedList<>();

        ModuleLink.getLinks().stream().sorted(Comparator.comparing(o -> o.getClass().getSimpleName())).forEachOrdered(link -> {
            View view = inflater.inflate(R.layout.row_suppported_module, moduleLayout, false);

            boolean isSupported = link.isSupported(account);
            CheckBox checkBox = view.findViewById(R.id.checkbox);
            checkBox.setChecked(isSupported);

            TextView textView = view.findViewById(R.id.text);
            textView.setText(link.getName());

            if (!isSupported && link.getRequirementText() != Resources.ID_NULL) {
                TextView requirementView = view.findViewById(R.id.requirement);
                requirementView.setText(link.getRequirementText());
                requirementView.setVisibility(View.VISIBLE);
            }

            ImageView icon = view.findViewById(R.id.icon);
            icon.setImageResource(link.getIcon());
            icon.setAlpha(isSupported? 1f : 0.5f);

            views.add(view);
        });

        // Add after all of them are configured, so that the findViewById calls above work correctly
        for (View view : views) {
            moduleLayout.addView(view);
        }
    }
}
