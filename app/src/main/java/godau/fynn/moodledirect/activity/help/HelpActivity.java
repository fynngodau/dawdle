package godau.fynn.moodledirect.activity.help;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.model.HelpItem;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.adapter.HelpAdapter;

import java.util.Arrays;

public class HelpActivity extends AppCompatActivity {

    private ViewPager2 viewPager;

    public static final String EXTRA_HELP_ITEM = "help_item";

    @Nullable
    private ViewPager2.OnPageChangeCallback onPageChangeCallback = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (MyApplication.getInstance().isDarkModeEnabled()) {
            setTheme(R.style.AppTheme_NoActionBar_Dark);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_help);

        setSupportActionBar(findViewById(R.id.toolbar));

        // Show action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.help);

        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(new HelpAdapter(this));

        if (savedInstanceState == null) {
            try {
                viewPager.setCurrentItem(HelpItem.valueOf(
                        new PreferenceHelper(this).getLastHelpMenuEntry()
                ).ordinal(), false);
            } catch (IllegalArgumentException e) {
                // Ignore, start at starting position
            }

            // Smooth scroll from last position to requested item
            if (getIntent().hasExtra(EXTRA_HELP_ITEM)) {
                viewPager.setCurrentItem(((HelpItem) getIntent().getSerializableExtra(EXTRA_HELP_ITEM)).ordinal());
            }
        }

        viewPager.requestFocus();

        TabLayout tabLayout = findViewById(R.id.tabs);
        if (tabLayout != null)
            new TabLayoutMediator(tabLayout, viewPager,
                    ((tab, position) -> tab.setText(HelpItem.values()[position].title))
            ).attach();
        else {
            ListView list = findViewById(R.id.list);
            list.setAdapter(
                    new ArrayAdapter<Object>(this, android.R.layout.simple_list_item_1,
                            Arrays.stream(HelpItem.values()).map(item -> getString(item.title)).toArray()) {
                        @NonNull
                        @Override
                        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                            TextView view = (TextView) super.getView(position, convertView, parent);
                            if (viewPager.getCurrentItem() == position) {
                                view.setTypeface(Typeface.DEFAULT_BOLD);
                            } else {
                                view.setTypeface(Typeface.DEFAULT);
                            }
                            return view;
                        }
                    }
            );
            list.setOnItemClickListener((parent, view, position, id) -> viewPager.setCurrentItem(position, true));

            findViewById(R.id.right).setOnClickListener(v -> navigate(true));
            findViewById(R.id.left).setOnClickListener(v -> navigate(false));

            viewPager.registerOnPageChangeCallback(onPageChangeCallback = new ViewPager2.OnPageChangeCallback() {
                @Override
                public void onPageSelected(int position) {
                    list.smoothScrollToPosition(position);
                    for (int first = list.getFirstVisiblePosition(), i = first; i <= list.getLastVisiblePosition(); i++) {
                        ((TextView) list.getChildAt(i - first)).setTypeface(
                                viewPager.getCurrentItem() == i? Typeface.DEFAULT_BOLD : Typeface.DEFAULT
                        );
                    }

                    super.onPageSelected(position);
                }
            });
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    /**
     * @param right True if the directional key "→" was pressed.
     */
    private void navigate(boolean right) {
        boolean ltr = viewPager.getLayoutDirection() == View.LAYOUT_DIRECTION_LTR;

        /* ltr & right => +1
         * ltr & left => -1
         * rtl & right => -1
         * rtl & left => +1
         * Therefore: +1 <=> ltr === right <=> rtl === left
         */
        viewPager.setCurrentItem(
                viewPager.getCurrentItem() + (ltr == right? +1 : -1), true
        );

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            navigate(true);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
            navigate(false);
            return true;
        } else return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        new PreferenceHelper(this).setLastHelpMenuEntry(HelpItem.values()[viewPager.getCurrentItem()].name());
        if (onPageChangeCallback != null) viewPager.unregisterOnPageChangeCallback(onPageChangeCallback);
        super.onDestroy();
    }
}
