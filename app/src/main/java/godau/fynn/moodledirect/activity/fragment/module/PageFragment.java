package godau.fynn.moodledirect.activity.fragment.module;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.SwipeRefreshFragment;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.ImageLoaderTextView;

import java.util.Arrays;

public class PageFragment extends SwipeRefreshFragment {

    private String url;
    private String[] files;
    private ImageLoaderTextView textView;

    private DisplayMetrics metrics;

    public PageFragment() {
    }

    public static PageFragment newInstance(String[] files) {

        // Eliminate forcedownloads before they are incorrectly encoded
        for (int i = 0; i < files.length; i++) {
            if (files[i].contains("?forcedownload=1")) {
                files[i] = files[i].replace("?forcedownload=1", "");
            }
        }

        PageFragment fragment = new PageFragment();
        Bundle bundle = new Bundle();
        bundle.putStringArray("files", files);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        files = getArguments().getStringArray("files");

        url = files[0];
        if (files.length > 1) {

            for (String file : files) {
                if (file.contains("content/index.html"))
                    url = file;
            }
        }

        Log.d("PAGE", url);
    }

    @Nullable
    @Override
    public View onCreateContentView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        textView = view.findViewById(R.id.textView);

        textView.setMovementMethod(LinkMovementMethod.getInstance());

        // DisplayMetrics for scaling images
        metrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
    }

    @Override
    protected void loadData(MoodleDatabase.Dispatch dispatch) {

        ExceptionHandler.tryAndThenThread(
                () -> dispatch.getPage().getPage(url),
                responseSpannable -> {

                    swipeRefreshLayout.setRefreshing(false);
                    empty.hide();
                    textView.setText(
                            responseSpannable, Arrays.asList(files), dispatch.getCommonAsset(), metrics
                    );
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                },
                failure -> {
                    empty.exception(failure);
                    swipeRefreshLayout.setRefreshing(false);
                },
                requireContext()
        );
    }
}
