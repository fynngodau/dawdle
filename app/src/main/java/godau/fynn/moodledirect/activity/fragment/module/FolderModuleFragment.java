package godau.fynn.moodledirect.activity.fragment.module;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.module.FileManager;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.DownloadItemViewHolder;
import godau.fynn.moodledirect.view.NoDataView;
import godau.fynn.moodledirect.view.adapter.course.ModuleHandler;
import godau.fynn.typedrecyclerview.SimpleRecyclerViewAdapter;

import java.util.List;

/**
 * Folders <b>don't</b> pull-to-refresh because they only use offline data.
 */
public class FolderModuleFragment extends Fragment {

    private static final String MODULE_INSTANCE_KEY = "moduleInstance";

    private int moduleInstance = 0;

    private FolderModuleFragment.FolderModuleAdapter adapter;
    private MoodleDatabase moodle;

    private NoDataView empty;

    public static FolderModuleFragment newInstance(int moduleId) {
        FolderModuleFragment fragment = new FolderModuleFragment();
        Bundle args = new Bundle();
        args.putInt(MODULE_INSTANCE_KEY, moduleId);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            moduleInstance = getArguments().getInt(MODULE_INSTANCE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_folder, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        empty = view.findViewById(R.id.empty);

        moodle = MyApplication.moodle();
        FileManagerWrapper fileManager = moodle.getDispatch().getFile().wrap();

        RecyclerView recyclerView = view.findViewById(R.id.files);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new FolderModuleFragment.FolderModuleAdapter(fileManager);
        recyclerView.setAdapter(adapter);

        Activity context = getActivity();

        // Load module data
        ExceptionHandler.tryAndThenThread(
                () -> moodle.forceOffline().getCourseContent().getModule(moduleInstance, ResourceType.FOLDER),
                module -> {
                    if (module == null) {
                        // This should never happen
                        Toast.makeText(getContext(), R.string.folder_missing_data, Toast.LENGTH_LONG).show();
                        return;
                    }

                    assert module.getModuleType() == ResourceType.FOLDER;

                    if (getActivity() != null) {
                        getActivity().setTitle(module.getName());
                    }

                    if (module.getFileList().isEmpty()) empty.text(R.string.folder_empty);
                    else empty.hide();

                    // Load course data
                    ExceptionHandler.tryAndThenThread(
                            () -> moodle.forceOffline().getCore().getCourse(module.courseId),
                            course -> adapter.setModule(course, module),
                            context
                    );
                },
                getContext()
        );

    }

    private static class FolderModuleAdapter extends SimpleRecyclerViewAdapter<File, DownloadItemViewHolder> {

        private final FileManagerWrapper fileManager;
        private Module module;
        private Course course;

        public FolderModuleAdapter(FileManagerWrapper fileManager) {
            this.fileManager = fileManager;
        }

        public void setModule(Course course, Module module) {
            content.clear();
            content.addAll(module.contents);
            notifyDataSetChanged();

            this.course = course;
            this.module = module;
        }

        public List<File> getContents() {
            return content;
        }

        @Override @NonNull
        public DownloadItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new DownloadItemViewHolder(
                    (ViewGroup) inflater.inflate(R.layout.row_file, parent, false)
            );
        }

        @Override
        public void onBindViewHolder(@NonNull DownloadItemViewHolder holder, File item, int position) {

            holder.name.setText(item.filename);

            int icon = FileManager.getIconFromFileName(item.filename);
            if (icon != Resources.ID_NULL) {
                holder.icon.setImageResource(icon);
            } else {
                holder.icon.setImageResource(R.drawable.ic_file_generic);
            }

            holder.downloadIcon.setTag(item);
            holder.displayDownloadStatus(item, false);

            holder.itemView.setOnClickListener((v) -> {
                switch (item.downloadStatus) {
                    case NOT_DOWNLOADED:
                    case FAILED:
                    case UPDATE_AVAILABLE: // TODO: delete old file
                        if (fileManager.startDownload(item, course.shortname, module.getName(), context, uri -> {

                            if (item.equals(holder.downloadIcon.getTag())) {

                                // Refresh download status of all visible views
                                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerViews.get(0).getLayoutManager();

                                for (int i = layoutManager.findFirstVisibleItemPosition();
                                     i <= layoutManager.findLastVisibleItemPosition(); i++
                                ) {
                                    RecyclerView.ViewHolder childHolder = recyclerViews.get(0).getChildViewHolder(
                                            layoutManager.findViewByPosition(i)
                                    );

                                    if (childHolder instanceof DownloadItemViewHolder) {
                                        ((DownloadItemViewHolder) childHolder).displayDownloadStatus(
                                                (content.get(i)), true
                                        );
                                    }
                                }

                            } else {
                                Log.d(ModuleHandler.class.getSimpleName(), "Item was reassigned, not displaying new status");
                            }

                        })) {
                            // Display that download is running
                            holder.displayDownloadStatus(item, true);
                        }
                        break;
                    case DOWNLOADING:
                        // TODO Show download status
                        break;
                    case DOWNLOADED:
                        fileManager.openFile(item, () -> holder.displayDownloadStatus(item, true), context);
                        break;
                }

            });
        }
    }
}
