package godau.fynn.moodledirect.activity.login.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Random;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.login.LoginActivity;
import godau.fynn.moodledirect.model.api.base.PublicConfig;
import godau.fynn.moodledirect.util.ScrollableFragment;

public class BrowserFragment extends ScrollableFragment {

    private static final String EXTRA_CONFIG = "config";

    private PublicConfig config;

    public static BrowserFragment get(PublicConfig config) {
        BrowserFragment fragment = new BrowserFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(EXTRA_CONFIG, config);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        config = (PublicConfig) getArguments().getSerializable(EXTRA_CONFIG);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ScrollView container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login_browser, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Button browserOpen = view.findViewById(R.id.browserOpen);
        LinearLayout browserOpenCredentials = view.findViewById(R.id.browserOpenCredentials);
        LinearLayout browserOpenToken = view.findViewById(R.id.browserOpenToken);

        browserOpen.setOnClickListener(v -> {
            Random random = new Random();
            String rand = String.valueOf(random.nextInt(1000));
            String url = config.launchurl + "?service=moodle_mobile_app&passport=" + rand + "&urlscheme=moodledirect";

            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));

            try {
                startActivity(i);
            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
                ((LoginActivity) requireActivity()).getAlertDialog(R.string.login_error_no_browser_installed)
                        .show();
            }
        });

        browserOpen.requestFocus();

        browserOpenCredentials.setOnClickListener(v -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                        R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.fragment_container, CredentialsFragment.get(config))
                .addToBackStack(null)
                .commit()
        );

        browserOpenToken.setOnClickListener(v -> requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .setReorderingAllowed(true)
                .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                        R.anim.slide_in_left, R.anim.slide_out_right)
                .replace(R.id.fragment_container, TokenFragment.get(config))
                .addToBackStack(null)
                .commit()
        );

    }
}
