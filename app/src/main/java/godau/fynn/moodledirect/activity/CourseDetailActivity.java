package godau.fynn.moodledirect.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.AutoTransition;
import androidx.transition.ChangeBounds;
import androidx.transition.TransitionManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.squareup.picasso.*;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.CourseEnrolFragment;
import godau.fynn.moodledirect.activity.fragment.CourseFragment;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.network.NetworkStateReceiver;
import godau.fynn.moodledirect.util.ExceptionHandler;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.view.NoDataView;

import java.util.List;

import static godau.fynn.moodledirect.util.Constants.EXTRA_COURSE;
import static godau.fynn.moodledirect.util.FileManagerWrapper.REQUEST_OPEN_DIRECTORY;

public class CourseDetailActivity extends AppCompatActivity
    implements NetworkStateReceiver.OfflineStatusChangeListener, NoDataView.InvokeListener {

    public static final String COURSE_ENROL_FRAG_TRANSACTION_KEY = "course_enrol_frag";

    private TextView offlineIndicator;
    private ViewGroup root;

    private AppBarLayout appBarLayout;
    private View scrim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (MyApplication.getInstance().isDarkModeEnabled()) {
            setTheme(R.style.AppTheme_NoActionBar_Dark_TranslucentStatus);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        appBarLayout = findViewById(R.id.appBar);
        scrim = findViewById(R.id.scrim);

        offlineIndicator = findViewById(R.id.offlineIndicator);
        root = findViewById(R.id.root);

        Intent intent = getIntent();

        /* We are getting the course to display either via ID (→ already enrolled)
         * or via serializable extra, in which case the user is not enrolled to the course.
         */

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();


        if (intent.hasExtra(EXTRA_COURSE)) {
            Course course = (Course) intent.getSerializableExtra(EXTRA_COURSE);
            setCourse(course);

            if (savedInstanceState == null) {
                Fragment fragment = CourseEnrolFragment.newInstance(course);
                fragmentTransaction.replace(
                        R.id.course_activity_frame,
                        fragment, null
                ).commit();
            }
        } else {
            int courseId = intent.getIntExtra("id", -1);

            if (courseId == -1) throw new IllegalArgumentException();

            ExceptionHandler.tryAndThenThread(
                    () -> MyApplication.moodle().forceOffline().getCore().getCourse(courseId),
                    course -> {
                        if (savedInstanceState == null) {
                            Fragment fragment = CourseFragment.newInstance(course);
                            fragmentTransaction.replace(
                                    R.id.course_activity_frame,
                                    fragment, null
                            ).commit();
                        }

                        setCourse(course);

                    },
                    CourseDetailActivity.this
            );
        }

        // Linearly increase opaqueness of scrim (the view that darkens image)
        appBarLayout.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
            float alpha = Math.abs(verticalOffset / (appBarLayout.getTotalScrollRange() * 0.5f));
            scrim.setAlpha(alpha * 0.125f + 0.1875f);
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        ((MyApplication) getApplication()).networkStateReceiver.addListener(this);
    }

    @Override
    public void onNewOfflineStatus(boolean offline) {
        if (offline) {
            TransitionManager.beginDelayedTransition(root, new ChangeBounds());
            offlineIndicator.setVisibility(View.VISIBLE);
        } else {
            TransitionManager.beginDelayedTransition(root, new AutoTransition());
            offlineIndicator.setVisibility(View.GONE);
        }
    }

    /*private void setCourseEnrol() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        mCourseEnrolFragment = CourseEnrolFragment.newInstance(TOKEN, mEnrolCourse);
        fragmentTransaction.replace(
                R.id.course_activity_frame,
                mCourseEnrolFragment,
                COURSE_ENROL_FRAG_TRANSACTION_KEY);
        fragmentTransaction.commit();
    }*/

    /**
     * Called by child fragments after the course data has been loaded.
     */
    private void setCourse(Course course) {
        setTitle(course.shortname);
        ((CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar)).setTitle(course.name);

        // Add course header

        ImageView imageView = findViewById(R.id.header);

        if (course.headerImage != null && !course.headerImage.url.equals(imageView.getTag())) {
            PreferenceHelper preferences = new PreferenceHelper(this);
            File headerFile = course.headerImage;
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageDrawable(null);
            imageView.setTag(headerFile.url);
            imageView.post(() -> {
                RequestCreator creator =
                        Picasso.get().load(Uri.parse(
                                        headerFile.url + "?token=" + preferences.getUserAccount().getToken()
                                )).fit().centerCrop()
                                .memoryPolicy(MemoryPolicy.NO_STORE);
                if (preferences.isForceOfflineModeEnabled()) {
                    creator.networkPolicy(NetworkPolicy.OFFLINE);
                }
                creator.into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        imageView.setTag(null);
                    }
                });
            });
            scrim.setVisibility(View.VISIBLE);

            // Warning: manual theming
            offlineIndicator.setBackgroundColor(getResources().getColor(R.color.gray26));
            findViewById(R.id.collapsing_toolbar).setBackground(null);
            getWindow().setStatusBarColor(getResources().getColor(R.color.systemBarScrim));

            appBarLayout.setBackground(null);
        } else if (course.headerImage == null) {
            imageView.setVisibility(View.GONE);
            scrim.setVisibility(View.GONE);
            offlineIndicator.setBackground(null);
            // This intentionally does not adapt to dark theme colors
            findViewById(R.id.collapsing_toolbar).setBackgroundColor(
                    getResources().getColor(R.color.colorPrimary)
            );
            appBarLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onInvokeNoData() {
        appBarLayout.setExpanded(false, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == REQUEST_OPEN_DIRECTORY && resultCode == Activity.RESULT_OK) {
            FileManagerWrapper.onRequestPermissionResult(data, this);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
