package godau.fynn.moodledirect.network;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import godau.fynn.moodledirect.model.api.MoodleException;
import godau.fynn.moodledirect.network.exception.NotOkayException;
import okhttp3.Interceptor;
import okhttp3.Response;

import java.io.IOException;
import java.util.List;

public class Validator implements Interceptor {

    private static final Gson gson = new Gson();

    Validator() {}

    /**
     * Throws if response is invalid. Works otherwise.
     */
    public static void validate(Response response) throws IOException {

        if (!response.isSuccessful()) {
            throw new NotOkayException();
        }

        if (response.body() == null) throw new IOException("Response has no body");

        String responseString = response.peekBody(4 * 1024 /* 4 KiB */).string();

        // Avoid EOFException because of strings that end unexpectedly
        if (responseString.getBytes().length < 4 * 1024) {

            // For validating exceptions in multi-query requests
            if (responseString.startsWith("[") && responseString.endsWith("]")) {
                    List<MultiQueryResponseItem> list = gson.fromJson(
                            responseString,
                            new TypeToken<List<MultiQueryResponseItem>>() {
                            }.getType()
                    );

                    if (list.size() > 0 && list.get(0).error) throw new NotOkayException();
            }

            if (responseString.startsWith("{") &&
                    responseString.getBytes().length < 4 * 1024) {
                MoodleException e = gson.fromJson(
                        responseString,
                        new TypeToken<MoodleException>() {
                        }.getType()
                );

                if (e.errorcode != null) throw e;
            }
        }

    }

    /**
     * Tests if response indicates success and returns it, throws otherwise
     *
     * @throws NotOkayException if response code does not indicate success
     * @throws MoodleException if moodle has returned an error
     * @throws IOException if an unknown network error occurs
     */
    @Override
    public Response intercept(Chain chain) throws IOException {

        Response response = chain.proceed(chain.request());

        validate(response);

        return response;
    }

    public static class MultiQueryResponseItem {
        boolean error;
        MoodleException exception;
    }
}
