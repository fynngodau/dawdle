package godau.fynn.moodledirect.data.persistence;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;
import godau.fynn.moodledirect.model.database.file.DownloadedFile;

@Dao
public abstract class DatabaseCleaner {

    @Query("SELECT * FROM downloadedFile")
    protected abstract DownloadedFile[] getDownloadedFiles();

    @Insert
    protected abstract void setDownloadedFiles(DownloadedFile[] files);

    @Transaction
    public Void clearDatabase(MoodleDatabase database) {
        // Persist files
        DownloadedFile[] files = getDownloadedFiles();

        database.clearAllTables();

        setDownloadedFiles(files);

        return null;
    }
}
