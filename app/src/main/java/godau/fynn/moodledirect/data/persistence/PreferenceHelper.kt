package godau.fynn.moodledirect.data.persistence

import android.content.Context
import android.content.SharedPreferences

/**
 * Load shared preferences centrally.
 * Preferences are set automatically by associated key through preference fragment.
 */
class PreferenceHelper(private val context: Context) {
    private val preferences: SharedPreferences =
        context.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)

    var userAccount: UserAccount
        /**
         * Gets currently logged in account.
         */
        get() = UserAccount(context, preferences.getString(KEY_CURRENT_ACCOUNT, null)!!)
        set(userAccount) {
            val preferenceValue = getPreferenceFileName(userAccount)
            preferences.edit()
                .putString(KEY_CURRENT_ACCOUNT, preferenceValue)
                .putStringSet(KEY_ACCOUNT_LIST, setOf(preferenceValue))
                .apply()
        }

    val isLoggedIn: Boolean
        /**
         * @return True if [.getUserAccount] may be called. Does not indicate whether token is valid.
         */
        get() = preferences.contains(KEY_CURRENT_ACCOUNT)

    fun logout() {
        userAccount.logout()
        preferences.edit()
            .remove(KEY_CURRENT_ACCOUNT)
            .putStringSet(KEY_ACCOUNT_LIST, emptySet())
            .apply()
    }

    val courseRowAppearance: CourseRowAppearance
        get() = CourseRowAppearance().apply {
            categoryAppearance = preferences.getString("appearance_category", "bottom")
            showHeaderImage = preferences.getBoolean("appearance_header", true)
            showUnreadCounter = preferences.getBoolean("unread", true)
            showDescription = preferences.getBoolean("appearance_description", true)
        }

    var downloadPath: String?
        get() = preferences.getString("download_path", null)
        set(uri) {
            preferences.edit()
                .putString("download_path", uri)
                .apply()
        }
    val isNotificationsEnabled: Boolean
        /**
         * Do not use yet, as notifications have not yet been implemented
         */
        get() = preferences.getBoolean("notifications", false)

    val isForceOfflineModeEnabled: Boolean
        /**
         * @return Whether user has requested offline mode to always be enabled.
         */
        get() = preferences.getBoolean("offline", false)

    val isDarkThemeEnabled: Boolean
        get() = preferences.getBoolean("dark_theme", false)

    val isAutoLoginEnabled: Boolean
        get() = preferences.getBoolean("web_auto_login", true)

    var lastHelpMenuEntry: String
        get() = preferences.getString("help_entry", "") ?: ""
        set(entry) {
            preferences.edit().putString("help_entry", entry).apply()
        }

    class CourseRowAppearance(
        @JvmField var categoryAppearance: String? = null,
        @JvmField var showHeaderImage: Boolean = false,
        @JvmField var showUnreadCounter: Boolean = false,
        @JvmField var showDescription: Boolean = false
    )

    companion object {
        const val SETTINGS_FILE = "godau.fynn.moodledirect:preferences"
        private const val KEY_CURRENT_ACCOUNT = "current_account"
        private const val KEY_ACCOUNT_LIST = "accounts"

        private fun getPreferenceFileName(userAccount: UserAccount): String {
            val domain = userAccount.url!!.replace("https?://".toRegex(), "")
                .replace("/".toRegex(), ".")
            return domain + ':' + userAccount.userId
        }
    }
}