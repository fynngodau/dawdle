package godau.fynn.moodledirect.data.persistence

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import godau.fynn.moodledirect.model.api.SiteInformation

/**
 * Initializes UserAccount based on data downloaded after logging in.
 * It is the caller's responsibility to call [PreferenceHelper.setUserAccount].
 */
fun SiteInformation.saveAsUserAccount(context: Context): UserAccount {
    val databaseName = siteUrl.replace("https?://".toRegex(), "")
        .replace("/".toRegex(), ".") + ':' + userId

    context.getSharedPreferences(databaseName, Context.MODE_PRIVATE).edit().apply {
        putString("username", username)
        putString("private_access_key", privateAccessKey)
        putString("name_first", firstName)
        putString("name_last", lastName)
        putString("avatar", avatar)
        putString("url", siteUrl)
        putInt("id", userId)

        // Copy supported calls
        putStringSet("instance_supported_calls",
            HashSet<String>().apply { addAll(supportedCalls.map { it.name }) }
        )

        token?.let {
            putString("token", it.token)
            it.privatetoken?.let { putString("token_private", token.privatetoken) }
        }

        apply()
    }

    return UserAccount(context, databaseName)
}

class UserAccount internal constructor(private val context: Context, val databaseName: String) {
    private val preferences: SharedPreferences =
        context.getSharedPreferences(databaseName, Context.MODE_PRIVATE)

    val token: String?
        get() = preferences.getString("token", "") // TODO defValue null

    fun hasPrivateToken(): Boolean = preferences.contains("token_private")

    val privateToken: String?
        get() = preferences.getString("token_private", null)
    val filePrivateAccessKey: String?
        get() = preferences.getString("private_access_key", "") // TODO defValue null
    val userLoginName: String?
        get() = preferences.getString("username", "") // TODO defValue null
    val userFirstName: String?
        get() = preferences.getString("name_first", "") // TODO defValue null
    val userId: Int
        get() = preferences.getInt("id", 0)

    /**
     * Gets API URL (in a normalized form starting with `http[s]://` and
     * ending with a trailing `/`).
     */
    val url: String?
        get() = preferences.getString("url", "")

    var moodleInstanceName: String?
        get() = preferences.getString("instance_name", "")
        set(name) {
            preferences.edit()
                .putString("instance_name", name)
                .apply()
        }
    val avatarUrl: String?
        get() = preferences.getString("avatar", "")

    val lastAutoLoginTime: Long
        get() = preferences.getLong("time_last_autologin", 0)

    fun recordAutoLogin() {
        preferences.edit()
            .putLong("time_last_autologin", System.currentTimeMillis())
            .apply()
    }

    var autoLoginCooldown: Long
        get() = preferences.getLong("autologin_cooldown", 360 * 1000L)
        set(cooldown) {
            preferences.edit()
                .putLong("autologin_cooldown", cooldown)
                .apply()
        }
    var supportedCalls: Set<String>?
        get() = preferences.getStringSet("instance_supported_calls", null)
        set(supportedCalls) {
            preferences.edit()
                .putStringSet("instance_supported_calls", supportedCalls)
                .apply()
        }

    @SuppressLint("ApplySharedPref")
    fun logout() {
        preferences.edit().clear().commit()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.deleteSharedPreferences(databaseName)
        }
    }
}