package godau.fynn.moodledirect.view.adapter.forum;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.R;

public class ForumViewHolder extends RecyclerView.ViewHolder {

    final ImageView avatar;
    final TextView subject;
    final TextView userName;
    final TextView modifiedTime;
    final TextView message;

    public ForumViewHolder(View itemView) {
        super(itemView);
        avatar = itemView.findViewById(R.id.user_avatar);
        subject = itemView.findViewById(R.id.subject);
        userName = itemView.findViewById(R.id.user_name);
        modifiedTime = itemView.findViewById(R.id.modified_time);
        message = itemView.findViewById(R.id.message);
    }
}
