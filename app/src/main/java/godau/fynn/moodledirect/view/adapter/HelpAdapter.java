package godau.fynn.moodledirect.view.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import godau.fynn.librariesdirect.AboutDirectFragment;
import godau.fynn.moodledirect.activity.help.fragment.HelpTextFragment;
import godau.fynn.moodledirect.model.HelpItem;
import godau.fynn.moodledirect.util.AboutDirectHelper;

import static godau.fynn.moodledirect.activity.help.fragment.HelpTextFragment.EXTRA_ITEM;

public class HelpAdapter extends FragmentStateAdapter {

    private Context context;

    public HelpAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        context = recyclerView.getContext();
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (HelpItem.values()[position].type == HelpItem.Type.TEXT) {
            Fragment fragment = new HelpTextFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(EXTRA_ITEM, HelpItem.values()[position]);
            fragment.setArguments(bundle);
            return fragment;
        } else {
            assert HelpItem.values()[position].type == HelpItem.Type.ABOUT;
            return AboutDirectHelper.getBuilder(context).buildFragment();
        }
    }

    @Override
    public int getItemCount() {
        return HelpItem.values().length;
    }
}
