package godau.fynn.moodledirect.view.adapter.course;

import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.CourseSection;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import godau.fynn.typedrecyclerview.TypeHandler;
import godau.fynn.typedrecyclerview.TypedRecyclerViewAdapter;

import java.util.List;

public class ModuleAdapter extends TypedRecyclerViewAdapter<Module> {

    private final ModuleHandler module, restrictedModule;

    public ModuleAdapter(FileManagerWrapper fileManager) {

        module = new ModuleHandler(fileManager);
        restrictedModule = new RestrictedModuleHandler(fileManager);
    }

    /**
     * Has priority over {@link #getItemHandler(Module, int)}, but may evaluate to <code>null</code>
     * to return {@link #module} from other method
     */
    @Override
    protected Class<? extends TypeHandler<?, ? extends Module>> getItemHandlerClass(Module item, int position) {
        if (item instanceof CourseSection)
            return CourseSectionHandler.class;
        else if (item.getModuleType() == ResourceType.LABEL)
            return LabelHandler.class;
        else
            return null;
    }

    @Override
    protected TypeHandler<?, ? extends Module> getItemHandler(Module item, int position) {
        if (item.available) {
            return module;
        } else {
            return restrictedModule;
        }
    }

    public void addModules(List<Module> modules) {

        int oldSize = getItemCount();

        content.addAll(modules);

        notifyItemRangeInserted(oldSize, getItemCount() - oldSize);

    }

    public void setCourse(Course course) {
        module.setCourse(course);
    }

    public void clear() {
        int oldSize = getItemCount();
        content.clear();
        notifyItemRangeRemoved(0, oldSize);
    }

    /**
     * Expose contents
     */
    public List<Module> getContent() {
        return content;
    }
}
