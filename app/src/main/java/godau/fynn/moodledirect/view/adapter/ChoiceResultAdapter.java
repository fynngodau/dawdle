package godau.fynn.moodledirect.view.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.List;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.model.api.choice.ChoiceResult;
import godau.fynn.moodledirect.model.api.choice.UserResponse;
import godau.fynn.moodledirect.network.NetworkStateReceiver;
import godau.fynn.typedrecyclerview.PrimitiveViewHolder;
import godau.fynn.typedrecyclerview.SimpleRecyclerViewAdapter;

public class ChoiceResultAdapter extends SimpleRecyclerViewAdapter<ChoiceResult, PrimitiveViewHolder<RecyclerView>> {

    public ChoiceResultAdapter(List<ChoiceResult> results) {
        super(results);
    }

    @NonNull
    @Override
    public PrimitiveViewHolder<RecyclerView> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PrimitiveViewHolder<>(
                (RecyclerView) inflater.inflate(R.layout.page_choice_result, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull PrimitiveViewHolder<RecyclerView> holder, ChoiceResult item, int position) {
        holder.itemView.setLayoutManager(new LinearLayoutManager(context));
        holder.itemView.setAdapter(new UserAdapter(item.users));
    }


    static class UserAdapter extends SimpleRecyclerViewAdapter<UserResponse, UserAdapter.ViewHolder> {

        public UserAdapter(List<UserResponse> users) {
            super(users);
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(inflater.inflate(R.layout.row_choice_result_user, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, UserResponse item, int position) {
            holder.name.setText(item.fullname);

            // Load avatar, don't attempt to use network if offline
            String privateAccessKey = new PreferenceHelper(context).getUserAccount()
                    .getFilePrivateAccessKey();
            RequestCreator creator = Picasso.get().load(
                    item.profileimageurl
                            .replace("/webservice/pluginfile.php/", "/pluginfile.php/")
                            .replace("/pluginfile.php/",
                                    "/tokenpluginfile.php/" + privateAccessKey + "/"
                            )
            );
            if (NetworkStateReceiver.getOfflineStatus()) {
                creator.networkPolicy(NetworkPolicy.OFFLINE);
            }
            creator.into(holder.avatar);
        }

        static class ViewHolder extends RecyclerView.ViewHolder {

            final TextView name;
            final ImageView avatar;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.name);
                avatar = itemView.findViewById(R.id.avatar);
            }
        }
    }
}
