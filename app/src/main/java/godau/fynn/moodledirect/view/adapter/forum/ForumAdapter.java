package godau.fynn.moodledirect.view.adapter.forum;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.forum.Discussion;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ClickListener;

import java.util.List;

public class ForumAdapter extends CommonForumAdapter<ForumViewHolder> {

    private final ClickListener<DiscussionClickParcel> clickListener;

    public ForumAdapter(ClickListener<DiscussionClickParcel> clickListener, List<Discussion> discussions) {
        super(discussions);
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public ForumViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ForumViewHolder viewHolder = new ForumViewHolder(
                inflater.inflate(R.layout.row_forum, parent, false)
        );

        viewHolder.itemView.setOnClickListener(v -> {
            int layoutPosition = viewHolder.getLayoutPosition();
            clickListener.onClick(new DiscussionClickParcel(content.get(layoutPosition), viewHolder),
                    layoutPosition);
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ForumViewHolder holder, Discussion discussion, int position) {
        super.onBindViewHolder(holder, discussion, position);

        // No rich display here, it is only a preview
        holder.message.setText(TextUtil.fromHtml(discussion.message).toString().replaceAll("\n+", "\n").trim());
    }

    public static class DiscussionClickParcel {
        private DiscussionClickParcel(Discussion discussion, ForumViewHolder viewHolder) {
            this.discussion = discussion;
            this.viewHolder = viewHolder;
        }

        public final Discussion discussion;
        public final ForumViewHolder viewHolder;
    }
}
