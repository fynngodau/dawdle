package godau.fynn.moodledirect.view.adapter.course;

import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.database.CourseSection;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ImageLoaderTextView;
import godau.fynn.typedrecyclerview.TypeHandler;

public class CourseSectionHandler extends TypeHandler<CourseSectionHandler.CourseSectionViewHolder, CourseSection> {
    @Override
    public CourseSectionViewHolder createViewHolder(@NonNull ViewGroup parent) {
        return new CourseSectionViewHolder(inflater.inflate(R.layout.row_course_section, parent, false));
    }

    @Override
    public void bindViewHolder(@NonNull CourseSectionViewHolder holder, CourseSection item, int position) {
        holder.sectionName.setText(item.getName());

        if (!item.getSummary().isEmpty()) {
            holder.description.setMaxHeight(Integer.MAX_VALUE);
            holder.description.setEnabled(true);

            TextUtil.setTextExpandable(
                    holder.description, item.getExpandableTextDisplay(context, recyclerViews.get(0).getWidth()),
                    position, recyclerViews.get(0), context
            );

            holder.description.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            // Use description as placeholder (this setup is necessary for the ripple to work exactly like with labels)
            holder.description.setMaxHeight(holder.description.getPaddingBottom());
            holder.description.setText(null);
            holder.description.setEnabled(false);
        }

        holder.sectionName.setAlpha(item.available? 1f : 0.5f);
        holder.description.setAlpha(item.available? 1f : 0.5f);

        if (!item.available && item.notAvailableReason != null) {
            holder.restrictionLayout.setVisibility(View.VISIBLE);
            holder.restriction.setText(TextUtil.fromHtml(item.notAvailableReason, context, holder.restriction.getWidth()));
        } else {
            holder.restrictionLayout.setVisibility(View.GONE);
        }
    }

    static class CourseSectionViewHolder extends RecyclerView.ViewHolder {

        final TextView sectionName, restriction;
        final ImageLoaderTextView description;
        final LinearLayout restrictionLayout;

        public CourseSectionViewHolder(@NonNull View itemView) {
            super(itemView);

            sectionName = itemView.findViewById(R.id.sectionName);
            description = itemView.findViewById(R.id.description);
            restriction = itemView.findViewById(R.id.restriction);
            restrictionLayout = itemView.findViewById(R.id.restrictionLayout);
        }
    }
}
