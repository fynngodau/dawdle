package godau.fynn.moodledirect.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.transition.TransitionManager;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.module.FileManager;
import godau.fynn.moodledirect.network.NetworkStateReceiver;
import godau.fynn.typedrecyclerview.PrimitiveViewHolder;

import java.util.List;

import static godau.fynn.moodledirect.module.FileManager.DownloadStatus.DOWNLOADED;

/**
 * For view holders that include the {@code R.layout.include_download_icon} layout and want to use it to
 * display the download status of a Module or file.
 */
public class DownloadItemViewHolder extends PrimitiveViewHolder<ViewGroup> {

    public final TextView name;
    public final ImageView icon, downloadIcon;
    public final ProgressBar progressBar;

    public DownloadItemViewHolder(@NonNull ViewGroup itemView) {
        super(itemView);

        name = itemView.findViewById(R.id.fileName);
        icon = itemView.findViewById(R.id.fileIcon);
        downloadIcon = itemView.findViewById(R.id.downloadIcon);
        progressBar = itemView.findViewById(R.id.progressBar);
    }

    public void displayDownloadStatus(Module item, boolean transition) {

        // DownloadStatus is used even for non-file modules (downloaded files are displayed like any other module)
        FileManager.DownloadStatus status = DOWNLOADED;
        if (item.isDownloadable() && item.getModuleType() == ResourceType.FILE) {
            List<File> contents = item.getFileList();

            // Per Module.isDownloadable(), FILEs have exactly one downloadable file
            downloadIcon.setTag(item.contents.get(0));
            status = contents.get(0).downloadStatus;
        } else {
            downloadIcon.setTag(null);
        }

        displayDownloadStatus(status, transition);
    }

    public void displayDownloadStatus(File item, boolean transition) {
        displayDownloadStatus(item.downloadStatus, transition);
    }

    public void displayDownloadStatus(FileManager.DownloadStatus downloadStatus, boolean transition) {
        if (transition) {
            TransitionManager.beginDelayedTransition(itemView);
        }

        float modIconAlpha = 0f, progressBarAlpha = 0f;

        switch (downloadStatus) {
            case NOT_DOWNLOADED:
                modIconAlpha = 0.25f;
                downloadIcon.setVisibility(View.VISIBLE);
                downloadIcon.setImageResource(
                        NetworkStateReceiver.getOfflineStatus() ?
                                R.drawable.ic_cloud_off :
                                R.drawable.ic_download
                );
                progressBarAlpha = 0f;
                break;
            case DOWNLOADING:
                modIconAlpha = 0.25f;
                progressBarAlpha = 1f;
                downloadIcon.setVisibility(View.GONE);
                break;
            case DOWNLOADED:
                modIconAlpha = 1f;
                progressBarAlpha = 0f;
                downloadIcon.setVisibility(View.GONE);
                break;
            case UPDATE_AVAILABLE:
                modIconAlpha = 0.25f;
                progressBarAlpha = 0f;
                downloadIcon.setVisibility(View.VISIBLE);
                downloadIcon.setImageResource(R.drawable.ic_update);
                break;
            case FAILED:
                modIconAlpha = 0.25f;
                progressBarAlpha = 0f;
                downloadIcon.setVisibility(View.VISIBLE);
                downloadIcon.setImageResource(R.drawable.ic_error);
        }

        if (transition) {
            icon.animate().alpha(modIconAlpha);
            progressBar.animate().alpha(progressBarAlpha);
        } else {
            icon.setAlpha(modIconAlpha);
            progressBar.setAlpha(progressBarAlpha);
        }

    }
}
