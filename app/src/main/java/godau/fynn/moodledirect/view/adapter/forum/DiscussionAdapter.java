package godau.fynn.moodledirect.view.adapter.forum;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.model.api.forum.Discussion;
import godau.fynn.moodledirect.module.FileManager;
import godau.fynn.moodledirect.util.FileManagerWrapper;
import godau.fynn.moodledirect.util.MyApplication;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.util.Util;
import godau.fynn.moodledirect.view.DownloadItemViewHolder;
import godau.fynn.moodledirect.view.ImageLoaderTextView;

import java.util.Collections;
import java.util.List;

public class DiscussionAdapter extends CommonForumAdapter<DiscussionAdapter.ViewHolder> {

    private final FileManagerWrapper fileManager = MyApplication.moodle().getDispatch().getFile().wrap();
    private final String folderName;
    private final DisplayMetrics metrics;

    public DiscussionAdapter(List<Discussion> discussions, String folderName, DisplayMetrics metrics) {
        super(discussions);
        this.folderName = folderName;
        this.metrics = metrics;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.row_discussion, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, Discussion item, int position) {
        super.onBindViewHolder(((ForumViewHolder) holder), item, position);

        float dp8 = Util.dpToPx(8, context);
        int marginStart = (int) (dp8 * (item.depth + 1f));
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();
        layoutParams.setMarginStart(marginStart);
        layoutParams.setMarginEnd((int) dp8);

        DisplayMetrics localMetrics = new DisplayMetrics();
        localMetrics.widthPixels = (int) (metrics.widthPixels - marginStart - 3*dp8);
        localMetrics.density = metrics.density;

        holder.message.setText(
                TextUtil.fromHtml(item.message, context, localMetrics.widthPixels),
                Collections.emptyList(), // TODO add support for inline "semi-attached" images if it is not already working
                MyApplication.moodle().getDispatch().getCommonAsset(),
                localMetrics
        );


        if (item.attachments != null && item.attachments.size() > 0) {
            holder.divider.setVisibility(View.VISIBLE);
            holder.attachmentLayout.setVisibility(View.VISIBLE);
            holder.attachmentLayout.removeAllViews();

            for (final File attachment : item.getFileList()) {
                DownloadItemViewHolder attachmentView = new DownloadItemViewHolder((ViewGroup) inflater.inflate(
                        R.layout.row_file,
                        holder.attachmentLayout, false
                ));
                holder.attachmentLayout.addView(attachmentView.itemView);

                attachmentView.name.setText(attachment.filename);

                int icon = FileManager.getIconFromFileName(attachment.filename);
                if (icon != Resources.ID_NULL) {
                    attachmentView.icon.setImageResource(icon);
                } else {
                    attachmentView.icon.setImageResource(R.drawable.ic_file_generic);
                }
                attachmentView.displayDownloadStatus(attachment, false);

                attachmentView.itemView.setOnClickListener((v) -> {
                    switch (attachment.downloadStatus) {
                        case NOT_DOWNLOADED:
                        case FAILED:
                        case UPDATE_AVAILABLE: // TODO: delete old file
                            if (fileManager.startDownload(attachment, folderName, item.subject, context, uri -> {
                                attachmentView.displayDownloadStatus(attachment, true);
                            })) {
                                attachmentView.displayDownloadStatus(attachment, true);
                            }
                            break;
                        case DOWNLOADING:
                            // TODO Show download status
                            break;
                        case DOWNLOADED:
                            fileManager.openFile(attachment, () -> attachmentView.displayDownloadStatus(attachment, true), context);
                            break;
                    }
                });
            }
        } else {
            holder.divider.setVisibility(View.GONE);
            holder.attachmentLayout.setVisibility(View.GONE);
            holder.attachmentLayout.removeAllViews();
        }
    }

    static class ViewHolder extends ForumViewHolder {

        private final LinearLayout attachmentLayout;
        private final ImageLoaderTextView message;
        private final View divider;


        public ViewHolder(@NonNull View view) {
            super(view);
            attachmentLayout = view.findViewById(R.id.attachments);
            message = (ImageLoaderTextView) super.message;
            divider = view.findViewById(R.id.divider);
        }
    }
}
