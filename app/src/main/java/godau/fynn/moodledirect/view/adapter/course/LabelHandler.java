package godau.fynn.moodledirect.view.adapter.course;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.util.TextUtil;
import godau.fynn.moodledirect.view.ImageLoaderTextView;
import godau.fynn.typedrecyclerview.TypeHandler;

public class LabelHandler extends TypeHandler<LabelHandler.LabelViewHolder, Module> {
    @Override
    public LabelViewHolder createViewHolder(@NonNull ViewGroup parent) {
        return new LabelViewHolder(inflater.inflate(R.layout.row_course_label, parent, false));
    }

    @Override
    public void bindViewHolder(@NonNull LabelViewHolder holder, Module item, int position) {

        TextUtil.setTextExpandable(
                holder.textView, item.getExpandableTextDisplay(context, recyclerViews.get(0).getWidth()),
                position, recyclerViews.get(0), context
        );

        /* Labels do not display grayed out if they are not available, because "available" has not practical
         * meaning in the context of labels.
         */
    }

    static class LabelViewHolder extends RecyclerView.ViewHolder {

        final ImageLoaderTextView textView;

        public LabelViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.text);
        }
    }
}
