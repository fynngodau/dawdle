package godau.fynn.moodledirect.module.basic;

import godau.fynn.moodledirect.module.ModuleD;

import java.io.IOException;

public class Calendar extends ModuleD {

    public String getExportToken() throws IOException {
        return moodleServices.getCalendarExportToken(userAccount.getToken()).execute().body().token;
    }
}
