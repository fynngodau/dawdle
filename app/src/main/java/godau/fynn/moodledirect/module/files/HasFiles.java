package godau.fynn.moodledirect.module.files;

import godau.fynn.moodledirect.model.api.file.File;

import java.util.List;
import java.util.function.Supplier;

public interface HasFiles {

    List<File> getFileList();

    void setFileList(List<File> fileList);

    String getReference();
}
