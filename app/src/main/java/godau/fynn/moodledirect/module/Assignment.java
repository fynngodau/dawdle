package godau.fynn.moodledirect.module;

import androidx.room.Dao;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.assign.SubmissionStatus;

import java.io.IOException;

public abstract class Assignment extends ModuleD {

    public abstract SubmissionStatus getSubmissionStatus(int assignmentId) throws IOException;

    @Dao
    public abstract static class Online extends Assignment {
        @Override
        public SubmissionStatus getSubmissionStatus(int assignmentId) throws IOException {
            SubmissionStatus status = moodleServices.getSubmissionStatus(
                    userAccount.getToken(), assignmentId
            ).execute().body();

            // TODO: Save status to database

            return status;
        }
    }

    @Dao
    public abstract static class Offline extends Assignment {
        @Override
        public SubmissionStatus getSubmissionStatus(int assignmentId) {
            throw new OfflineException(R.string.feature_preview_offline);
        }
    }
}
