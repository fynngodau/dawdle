package godau.fynn.moodledirect.module.link;

import android.view.View;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.module.AssignmentFragment;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;

public class AssignmentLink extends ModuleLink {
    @Override
    protected void onOpen(Module module, Course course, FragmentActivity context, View drawContext) {
        showFragment(context,
                AssignmentFragment.newInstance(module),
                null
        );
    }

    @Override
    public int getIcon() {
        return R.drawable.book;
    }

    @Override
    public int getName() {
        return R.string.assignment;
    }

    @Nullable
    @Override
    protected String[] requiresCalls() {
        return new String[]{
                "mod_assign_get_submission_status",
        };
    }

    @Override
    public int getRequirementText() {
        return R.string.supported_modules_requirement_moodle_31;
    }
}
