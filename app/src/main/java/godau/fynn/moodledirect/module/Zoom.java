package godau.fynn.moodledirect.module;

import androidx.room.Dao;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;

import java.io.IOException;

@Dao
public abstract class Zoom extends ModuleD {

    public String getConferenceLink(int id) throws IOException, OfflineException {
        throw new OfflineException(R.string.exception_offline_zoom);
    }

    @Dao
    public static abstract class Online extends Zoom {

        @Override
        public String getConferenceLink(int id) throws IOException {
            return moodleServices.getZoom(id, userAccount.getToken()).execute().body().joinurl;
        }
    }
}
