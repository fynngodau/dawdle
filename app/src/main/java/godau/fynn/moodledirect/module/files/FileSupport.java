package godau.fynn.moodledirect.module.files;

import godau.fynn.moodledirect.model.api.file.File;

import java.util.LinkedList;
import java.util.List;

public class FileSupport {

    private FileSupport() {}

    public static List<File> buildFileList(List<HasFiles> hasFilesList) {
        // Assemble files
        List<File> fileList = new LinkedList<>();
        for (HasFiles has : hasFilesList) {
            if (has.getFileList() == null) continue;
            int orderNumber = 1;
            for (File file : has.getFileList()) {
                if (file.url == null) continue;
                file.reference = has.getReference();
                file.orderNumber = orderNumber++;
                fileList.add(file);
            }
        }
        return fileList;
    }
}
