package godau.fynn.moodledirect.module.link;

import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.module.PageFragment;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;

public class PageLink extends ModuleLink {
    @Override
    protected void onOpen(Module module, Course course, FragmentActivity context, View drawContext) {
        if (module.contents == null || module.contents.size() == 0) {
            Toast.makeText(context, R.string.page_not_available, Toast.LENGTH_SHORT).show();
            return;
        }

        String[] files = new String[module.getFileList().size()];

        for (int i = 0; i < files.length; i++) {
            files[i] = module.getFileList().get(i).url;
        }

        Fragment pageFragment = PageFragment.newInstance(files);
        showFragment(context, pageFragment, "Page");
    }

    @Override
    public int getIcon() {
        return R.drawable.page;
    }

    @Override
    public int getName() {
        return R.string.supported_modules_page;
    }
}
