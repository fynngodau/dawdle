package godau.fynn.moodledirect.module.link;

import android.view.View;

import androidx.fragment.app.FragmentActivity;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;

public class UrlLink extends ModuleLink {

    @Override
    protected void onOpen(Module module, Course course, FragmentActivity context, View drawContext) {
        if (module.getFileList().size() > 0 && !module.getFileList().get(0).url.isEmpty()) {
            // No automagic login for external URL
            ModuleLink.openWebsite(context, module.getFileList().get(0).url);
        }
    }

    @Override
    public int getIcon() {
        return R.drawable.web;
    }

    @Override
    public int getName() {
        return R.string.supported_modules_url;
    }
}
