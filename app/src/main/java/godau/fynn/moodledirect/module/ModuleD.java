package godau.fynn.moodledirect.module;

import android.content.Context;
import godau.fynn.moodledirect.data.persistence.MoodleDatabase;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.network.APIClient;
import godau.fynn.moodledirect.network.MoodleServices;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public abstract class ModuleD {

    protected Context context;
    protected PreferenceHelper preferences;
    protected UserAccount userAccount;
    protected MoodleServices moodleServices;
    protected MoodleDatabase.Dispatch database;

    public ModuleD inject(Context context, MoodleDatabase.Dispatch dispatch) {
        this.context = context;
        database = dispatch;

        preferences = new PreferenceHelper(context);
        userAccount = preferences.getUserAccount();
        moodleServices = APIClient.getServices();
        return this;
    }

    /**
     * @return Location where the provided URL should be downloaded to.
     */
    protected File getFileLocation(String url) {
        return new File(context.getCacheDir(), String.valueOf(url.hashCode()));
    }

    /**
     * Downloads the provided URL to the location specified by {@link #getFileLocation(String)}.
     *
     * @param sendToken Whether auth should be provided to the remote server. Set false if not our moodle server.
     * @return Location that the requested file is downloaded to.
     */
    protected File download(String url, boolean sendToken) throws IOException {
        File storageLocation = getFileLocation(url);

        ResponseBody responseBody = APIClient.getServices()
                .getFile(url, sendToken ? userAccount.getToken() : null).execute().body();

        BufferedSink sink = Okio.buffer(
                Okio.sink(
                        new FileOutputStream(storageLocation)
                )
        );

        sink.writeAll(responseBody.source());
        sink.close();
        responseBody.close();

        return storageLocation;
    }

}
