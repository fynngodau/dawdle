package godau.fynn.moodledirect.module;

import android.util.Log;
import androidx.annotation.Nullable;
import androidx.room.Dao;
import godau.fynn.moodledirect.activity.fragment.module.PageFragment;
import godau.fynn.moodledirect.module.files.AssetLoader;
import kotlin.io.encoding.Base64;
import okio.BufferedSink;
import okio.Okio;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Downloads inline images.
 */
@Dao
public abstract class CommonAsset extends ModuleD implements AssetLoader {

    /**
     * @param sendAuth Should be false for external sources in order not to leak authentication details
     */
    @Override
    @Nullable
    public File getAsset(String url, boolean sendAuth) throws IOException {

        // Extract "data:" resources

        // Try to load from cache

        File storageLocation = getFileLocation(url);

        if (storageLocation.isFile()) {
            Log.d(PageFragment.class.getSimpleName(), "Loading file " + url +
                    " from cache");
            return storageLocation;
        } else if (url.startsWith("data:") && url.contains(";base64") && url.split(";base64,").length > 1) {
            BufferedSink sink = Okio.buffer(Okio.sink(new FileOutputStream(storageLocation)));

            String base64Data = url.split(";base64,")[1];
            sink.write(Base64.Default.decode(base64Data, 0, base64Data.length()));
            sink.close();
            return storageLocation;
        } else return null;
    }

    @Dao
    public static abstract class Online extends CommonAsset {
        /**
         * @param sendAuth Should be false for external sources in order not to leak the token
         */
        @Nullable
        public File getAsset(String url, boolean sendAuth) throws IOException {

            // Prefer offline storage or cache
            File cacheFile = super.getAsset(url, sendAuth);
            if (cacheFile != null) return cacheFile;


            // Download file from URL
            return download(url, sendAuth);
        }
    }

}
