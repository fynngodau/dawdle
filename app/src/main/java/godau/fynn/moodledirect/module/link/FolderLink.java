package godau.fynn.moodledirect.module.link;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.activity.fragment.module.FolderModuleFragment;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;

public class FolderLink extends ModuleLink {
    @Override
    protected void onOpen(Module module, Course course, FragmentActivity context, View drawContext) {
        Fragment folderModuleFragment = FolderModuleFragment.newInstance(module.getInstance());
        showFragment(context, folderModuleFragment, "Folder");
    }

    @Override
    public int getIcon() {
        return R.drawable.folder;
    }

    @Override
    public int getName() {
        return R.string.folder;
    }
}
