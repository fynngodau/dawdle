package godau.fynn.moodledirect.module.forum;

import androidx.room.ColumnInfo;

/**
 * POJO that helps with merging Discussion objects in {@link Forum} module.
 */
public class DiscussionMergeHelper {
    public int id, lastPostUserId, replyCount;
    @ColumnInfo(name = "forumId") public int forumInstance;
    public boolean pinned;
}
