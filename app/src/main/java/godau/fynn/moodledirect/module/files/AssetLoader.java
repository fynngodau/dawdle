package godau.fynn.moodledirect.module.files;

import androidx.annotation.Nullable;

import java.io.File;
import java.io.IOException;

public interface AssetLoader {
    @Nullable
    File getAsset(String url, boolean sendAuth) throws IOException;
}
