package godau.fynn.moodledirect.module.link;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.data.persistence.PreferenceHelper;
import godau.fynn.moodledirect.data.persistence.UserAccount;
import godau.fynn.moodledirect.model.ResourceType;
import godau.fynn.moodledirect.model.database.Course;
import godau.fynn.moodledirect.model.database.Module;
import godau.fynn.moodledirect.util.AutoLoginHelper;

import static godau.fynn.moodledirect.model.ResourceType.*;

/**
 * Module links deal handle events that should open a given module; they instantiate
 * appropriate fragments or similar.
 */
public abstract class ModuleLink {

    private static final Map<ResourceType, Class<? extends ModuleLink>> LINKS;
    private static Map<ResourceType, ModuleLink> links;

    static {

        Map<ResourceType, Class<? extends ModuleLink>> links = new HashMap<>();

        links.put(CHOICE, ChoiceLink.class);
        links.put(FOLDER, FolderLink.class);
        links.put(FORUM, ForumLink.class);
        links.put(PAGE, PageLink.class);
        links.put(URL, UrlLink.class);
        links.put(ZOOM, ZoomLink.class);
        links.put(ASSIGNMENT, AssignmentLink.class);

        LINKS = Collections.unmodifiableMap(links);

        /* links with small letters is not instantiated here because this would
         * result in a class instantiation deadlock.
         */

    }

    private static void instantiate() {
        links = new HashMap<>();
        LINKS.keySet().stream()
                .forEach(resourceType -> {
                    try {
                        links.put(resourceType,
                                LINKS.get(resourceType).getConstructor().newInstance()
                        );
                    } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                });
    }

    public static Collection<ModuleLink> getLinks() {
        if (links == null) {
            instantiate();
        }

        return links.values();
    }

    public static void openWebsite(Context context, String url) {

        CustomTabsIntent intent = new CustomTabsIntent.Builder()
                .build();

        intent.launchUrl(context, Uri.parse(url));
    }

    public static void open(Module module, Course course, FragmentActivity context, View drawContext) {

        if (links == null) {
            instantiate();
        }

        ModuleLink link = links.get(module.getModuleType());
        if (link != null && link.isSupported(new PreferenceHelper(context).getUserAccount())) {
                link.onOpen(module, course, context, drawContext);
        } else {
            AutoLoginHelper.openWithAutoLogin(context, drawContext, module.url);
        }
    }

    protected abstract void onOpen(Module module, Course course, FragmentActivity context, View drawContext);

    /**
     * @param account Identifies the Moodle instance for which support should be checked.
     * @return False if this module cannot be used in the app with this Moodle instance.
     */
    public boolean isSupported(UserAccount account) {

        String[] requiresCalls = requiresCalls();
        if (requiresCalls == null) return true;

        Set<String> supportedCalls = account.getSupportedCalls();
        if (supportedCalls == null) return true; // Migration post update to the first version that supported this

        for (String call : requiresCalls) {
            boolean callSupported = supportedCalls.contains(call);
            Log.d(ModuleLink.class.getSimpleName() + " required calls", (supportedCalls.contains(call)? '✔' : '❌') + " " + call);
            if (!callSupported) return false;
        }
        return true;
    }

    /**
     * @return An array of moodle API calls required for this module to function. Module should not
     * be called if one of these functions is not available.
     */
    protected @Nullable String[] requiresCalls() { return null; }

    public static @DrawableRes int getIcon(Module module) {
        if (links == null) {
            instantiate();
        }

        ModuleLink link = links.get(module.getModuleType());
        if (link == null) return Resources.ID_NULL;
        else return link.getIcon();
    }

    public abstract @DrawableRes int getIcon();

    public abstract @StringRes int getName();

    public @StringRes int getRequirementText() {
        return Resources.ID_NULL;
    }

    protected void showFragment(FragmentActivity activity, Fragment fragment, String tag) {
        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .replace(R.id.course_activity_frame, fragment, tag);
        fragmentTransaction.commit();
    }
}
