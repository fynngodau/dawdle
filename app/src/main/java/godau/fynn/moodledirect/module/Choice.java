package godau.fynn.moodledirect.module;

import static androidx.room.OnConflictStrategy.REPLACE;

import androidx.room.*;
import godau.fynn.moodledirect.OfflineException;
import godau.fynn.moodledirect.R;
import godau.fynn.moodledirect.model.api.choice.ChoiceDetails;
import godau.fynn.moodledirect.model.api.choice.ChoiceDetailsList;
import godau.fynn.moodledirect.model.api.choice.ChoiceOption;
import godau.fynn.moodledirect.model.api.choice.ChoiceResult;
import godau.fynn.moodledirect.model.api.file.File;
import godau.fynn.moodledirect.module.files.FileSupport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public abstract class Choice extends ModuleD {

    /**
     * Parallel execution of {@link #getChoiceDetails(int, int)} and {@link #getChoiceOptions(int)}.
     */
    public ChoiceObject getChoiceObject(int choiceId, int courseId) throws IOException {

        List<IOException> ioExceptionList = new ArrayList<>();
        List<OfflineException> offlineExceptionList = new ArrayList<>();
        ChoiceObject choice = new ChoiceObject();
        Thread detailThread = new Thread(() -> {
            try {
                choice.details = getChoiceDetails(choiceId, courseId);
            } catch (IOException e) {
                e.printStackTrace();
                ioExceptionList.add(e);
            } catch (OfflineException e) {
                e.printStackTrace();
                offlineExceptionList.add(e);
            }
        });

        Thread optionsThread = new Thread(() -> {
            try {
                choice.options = getChoiceOptions(choiceId);
            } catch (IOException e) {
                e.printStackTrace();
                ioExceptionList.add(e);
            } catch (OfflineException e) {
                e.printStackTrace();
                offlineExceptionList.add(e);
            }
        });

        detailThread.start();
        optionsThread.start();

        try {
            detailThread.join();
            optionsThread.join();
        } catch (InterruptedException e) {
            throw new IOException("Network operations were interrupted");
        }

        if (!ioExceptionList.isEmpty()) throw ioExceptionList.get(0);
        else if (!offlineExceptionList.isEmpty()) throw offlineExceptionList.get(0);
        else return choice;

    }

    protected abstract ChoiceDetails getChoiceDetails(int choiceId, int courseId) throws IOException;

    protected abstract List<ChoiceOption> getChoiceOptions(int choiceId) throws IOException;

    /**
     * @param optionIds IDs of options that were chosen by the user
     * @return {@link Void} because {@code tryAndThen} doesn't work otherwise as it can't defer the return type
     */
    public abstract Void setChosenOptions(int choiceId, List<Integer> optionIds) throws IOException;

    public abstract Void undoChoice(int choiceId) throws IOException;

    public abstract List<ChoiceResult> getChoiceResults(int choiceId) throws IOException;

    @Dao
    public abstract static class Online extends Choice {

        @Insert(onConflict = REPLACE)
        protected abstract void insertChoiceDetails(List<ChoiceDetails> choiceDetails);

        @Insert(onConflict = REPLACE)
        protected abstract void insertFiles(List<File> files);

        @Insert(onConflict = REPLACE)
        protected abstract void insertChoiceOptions(List<ChoiceOption> choiceOptions);

        @Insert(onConflict = REPLACE)
        protected abstract void insertChoiceResults(List<ChoiceResult> choiceResults);

        @Query("UPDATE choiceOption SET checked = 0 WHERE reference = :choiceId")
        protected abstract void resetCheckedOptions(int choiceId);

        @Query("UPDATE choiceOption SET checked = 1 WHERE id IN (:chosenOptionIds)")
        protected abstract void setChosenByIds(List<Integer> chosenOptionIds);

        @Override
        @Transaction // rollback database changes if exception occurs
        public ChoiceDetails getChoiceDetails(int choiceId, int courseId) throws IOException {
            ChoiceDetailsList list = moodleServices.getChoiceDetails(userAccount.getToken(), courseId).execute().body();

            // Save all
            insertChoiceDetails(list.choices);
            insertFiles(FileSupport.buildFileList(new ArrayList<>(list.choices)));

            for (ChoiceDetails details : list.choices) {
                if (details.id == choiceId) return details;
            }

            throw new IOException("Could not find the requested details in the objects returned by server");
        }

        @Override
        protected List<ChoiceOption> getChoiceOptions(int choiceId) throws IOException {
            List<ChoiceOption> options = moodleServices.getChoiceOptions(userAccount.getToken(), choiceId)
                    .execute().body().options;

            int i = 0;
            for (ChoiceOption option : options) {
                option.reference = choiceId;
                option.orderNumber = ++i;
            }

            insertChoiceOptions(options);
            return options;
        }

        @Override
        @Transaction
        public Void setChosenOptions(int choiceId, List<Integer> optionIds) throws IOException {

            // Build appropriate query parameters
            LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
            for (int i = 0; i < optionIds.size(); i++) {
                map.put("responses[" + i + "]", optionIds.get(i));
            }

            moodleServices.submitChoiceResponse(userAccount.getToken(), choiceId, map).execute();
            // If this is not successful, the validator interceptor throws an exception.

            // Store change to database
            resetCheckedOptions(choiceId);
            setChosenByIds(optionIds);

            return null;
        }

        @Override
        public Void undoChoice(int choiceId) throws IOException {
            moodleServices.undoChoiceResponse(userAccount.getToken(), choiceId).execute();
            // If this is not successful, the validator interceptor throws an exception.

            resetCheckedOptions(choiceId);
            return null;
        }

        @Override
        public List<ChoiceResult> getChoiceResults(int choiceId) throws IOException {
            List<ChoiceResult> results = moodleServices
                    .getChoiceResults(userAccount.getToken(), choiceId)
                    .execute().body()
                    .options;

            int i = 0;
            for (ChoiceResult result : results) {
                result.reference = choiceId;
                result.orderNumber = ++i;
            }

            insertChoiceResults(results);
            return results;
        }
    }

    @Dao
    public abstract static class Offline extends Choice {

        @Query("SELECT * FROM choiceResult WHERE reference = :choiceId ORDER BY orderNumber ASC")
        public abstract List<ChoiceResult> getChoiceResults(int choiceId);

        @Query("SELECT * FROM choice WHERE id == :choiceId")
        protected abstract ChoiceDetails getChoiceDetails(int choiceId);

        @Query("SELECT * FROM file WHERE reference = 'choice' || :choiceId ORDER BY orderNumber ASC")
        protected abstract List<File> getFilesForChoiceDetails(int choiceId);

        @Query("SELECT * FROM choiceOption WHERE reference = :choiceId ORDER BY orderNumber ASC")
        protected abstract List<ChoiceOption> getChoiceOptions(int choiceId);

        @Override
        @Transaction
        public ChoiceDetails getChoiceDetails(int choiceId, int courseId) {
            ChoiceDetails details = getChoiceDetails(choiceId);
            if (details == null) throw new OfflineException();
            details.descriptionFiles = getFilesForChoiceDetails(choiceId);
            return details;
        }

        @Override
        public Void setChosenOptions(int choiceId, List<Integer> optionIds) {
            throw new OfflineException(R.string.exception_offline_choice_submit);
        }

        @Override
        public Void undoChoice(int choiceId) {
            throw new OfflineException(R.string.exception_offline_choice_undo);
        }
    }

    public static class ChoiceObject {
        public ChoiceDetails details;
        public List<ChoiceOption> options;
    }
}
