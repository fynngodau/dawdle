package godau.fynn.moodledirect.module.basic;

import godau.fynn.moodledirect.model.api.tool.AutoLogin;
import godau.fynn.moodledirect.model.api.tool.MobileConfig;
import godau.fynn.moodledirect.module.ModuleD;

import java.io.IOException;

/**
 * Responsible for API calls starting with <code>tool_</code>
 */
public class Tool extends ModuleD {

    public String getAutoLoginUrl(String targetUrl) throws IOException {

        AutoLogin autoLogin = moodleServices.getAutoLoginKey(
                userAccount.getToken(), userAccount.getPrivateToken()
        ).execute().body();

        // Concatenate finalized URL
        return autoLogin.autologinurl + "?key=" + autoLogin.key
                + "&userid=" + userAccount.getUserId()
                + "&urltogo=" + targetUrl;

    }

    public MobileConfig getMobileConfig() throws IOException {
        return moodleServices.getMobileConfig(
                userAccount.getToken()
        ).execute().body();
    }
}
