package crux.bphc.cms.service;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

import static crux.bphc.cms.service.NotificationService.NOTIFICATION_CHANNEL_UPDATES;
import static crux.bphc.cms.service.NotificationService.NOTIFICATION_CHANNEL_UPDATES_BUNDLE;

public class NotificationChannelSupport {
    // Create channels for devices running Oreo and above; Can be safely called even if channel exists
    public static void createNotificationChannels(Activity context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the "Updates Bundle" channel, which is channel for summary notifications that bundles thr
            // individual notifications
            NotificationChannel service = new NotificationChannel(NOTIFICATION_CHANNEL_UPDATES_BUNDLE,
                    "New Content Bundle",
                    NotificationManager.IMPORTANCE_DEFAULT);
            service.setDescription("A default priority channel that bundles all the notifications");

            // Create the "Updates" channel which has the low importance level
            NotificationChannel updates = new NotificationChannel(NOTIFICATION_CHANNEL_UPDATES,
                    "New Content",
                    NotificationManager.IMPORTANCE_LOW);
            updates.setDescription("All low importance channel that relays all the updates.");

            NotificationManager nm = context.getSystemService(NotificationManager.class);
            // create both channels
            nm.createNotificationChannel(service);
            nm.createNotificationChannel(updates);
        }
    }
}
